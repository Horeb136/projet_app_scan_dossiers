DROP TABLE IF EXISTS scans;
DROP TABLE IF EXISTS file_information;

CREATE TABLE scans (
  scan_id INT AUTO_INCREMENT  PRIMARY KEY,
  repository VARCHAR(250) NOT NULL,
  max_files INT NOT NULL,
  max_depth INT NOT NULL,
  file_filter VARCHAR(250) NOT NULL,
  type_filter VARCHAR(250) NOT NULL,
  scan_date VARCHAR(250) NOT NULL,
  execution_time VARCHAR(250) NOT NULL
);

CREATE TABLE file_information (
  file_name VARCHAR(250),
  file_date_modification VARCHAR(20) NOT NULL,
  file_repository VARCHAR(250) NOT NULL,
  file_type VARCHAR(250) NOT NULL,
  file_size FLOAT NOT NULL,
  scan_id INT NOT NULL,
  PRIMARY KEY (scan_id, file_name),
  CONSTRAINT fk_scan FOREIGN KEY (scan_id) REFERENCES scan (scan_id)
);
