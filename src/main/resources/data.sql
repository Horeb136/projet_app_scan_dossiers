INSERT INTO scans (execution_time, file_filter, max_depth, max_files, repository, scan_date,  type_filter) VALUES
  ('3s', 'code', 4, 10, 'JAVA', '2024/02/11', 'txt'),
  ('4s', 'code', 4, 15, 'PYTHON', '2024/02/12', 'txt');


INSERT INTO file_information (file_name, scan_id, file_date_modification, file_repository, file_size, file_type) VALUES
  ('data', 1, '2023/12/31', 'JAVA', 5, 'csv'),
  ('metadata', 1, '2023/12/31', 'JAVA', 2, 'txt'),
  ('code', 1, '2023/12/31', 'JAVA', 1, 'py');


INSERT INTO scans (execution_time, file_filter, max_depth, max_files, repository, scan_date,  type_filter) VALUES
  ('5s', 'code', 2, 10, '/home/ensai/Documents/GENIE_LOGICIEL/DEVOPS/COURS', '2024/02/15', 'txt');