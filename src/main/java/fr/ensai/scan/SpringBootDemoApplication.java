package fr.ensai.scan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.ensai.scan.service.ScanService;

@SpringBootApplication
public class SpringBootDemoApplication {
	@Autowired
	public static ScanService scanService;
	@Autowired
	@Qualifier("scanService")
	public static ScanService scanService2;


	public static void main(String[] args) {
		
		SpringApplication.run(SpringBootDemoApplication.class, args);
		if(SpringBootDemoApplication.scanService == SpringBootDemoApplication.scanService2){
		}
	}

}
