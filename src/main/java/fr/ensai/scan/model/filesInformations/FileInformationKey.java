package fr.ensai.scan.model.filesInformations;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotEmpty;


@Embeddable
public class FileInformationKey implements Serializable {
    @NotEmpty(message = "File name missing")
    @Column(name = "file_name", insertable=false, updatable=false)
    private String fileName;

    @Column(name="scan_id", insertable = false, updatable = false)
    private Long scanId;

    public FileInformationKey() {
    }
    
    public FileInformationKey(String fileName, Long scanId) {
        this.fileName = fileName;
        this.scanId = scanId;
    }


    public String getFileName() {
        return fileName;
    }


    public Long getScanId() {
        return scanId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FileInformationKey)) return false;
        FileInformationKey that = (FileInformationKey) o;
        return Objects.equals(getFileName(), that.getFileName()) &&
                Objects.equals(getScanId(), that.getScanId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFileName(), getScanId());
    }

    @Override
    public String toString() {
        return "Name : " + this.fileName + ", Id : " + this.scanId;
    }
}
