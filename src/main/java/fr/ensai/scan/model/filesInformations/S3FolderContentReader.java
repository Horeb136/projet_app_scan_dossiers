package fr.ensai.scan.model.filesInformations;

import java.util.ArrayList;

import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.util.List;

import fr.ensai.scan.utils.DataFormatter;

public class S3FolderContentReader extends FileInformationNode {

    private String bucketName;
    private List<FileInformationNode> fileInformationNodes = new ArrayList<>();

    public S3FolderContentReader() {}
    
    public S3FolderContentReader(String name, String dateModification, String repository, String bucketName) {
        super(name, dateModification, repository);
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public List<FileInformationNode> getFileInformationNodes() {
        return fileInformationNodes;
    }

    public void addFile(FileInformationNode fileInformationNode) {
        this.fileInformationNodes.add(fileInformationNode);
    }

    @Override
    public String getSize() {
        long size = 0;
        for (FileInformationNode fileInformationNode: fileInformationNodes) {
            size += DataFormatter.deConvertSize(fileInformationNode.getSize());
        }
        return DataFormatter.convertSize(size);
    }

    public List<FileInformationNode> browseFolder(S3Client s3Client, ListObjectsV2Request listObjectsRequest) {

        List<FileInformationNode> fileInfoNodes = new ArrayList<>();

        ListObjectsV2Response listObjectsResponse = s3Client.listObjectsV2(listObjectsRequest);
        List<S3Object> filesAndFolders = listObjectsResponse.contents();

        if (filesAndFolders != null) {
            for (S3Object object : filesAndFolders) {
                long startFileTime = System.nanoTime();
                String key = object.key();
                String lastModified = object.lastModified().toString();
                if (key.endsWith("/")) {
                    String[] segments = key.split("/");
                    String name = key;
                    String parentDirectory = "";
                    if (segments.length != 1) {
                        StringBuilder parentDirectoryBuilder = new StringBuilder();
                        for (int i = 0; i < segments.length - 1; i++) {
                            parentDirectoryBuilder.append(segments[i]).append("/");
                        }
                        parentDirectory = parentDirectoryBuilder.toString();
                    }
                    S3FolderContentReader s3FolderContentReader = new S3FolderContentReader(name, lastModified, parentDirectory, this.bucketName);
                    fileInfoNodes.add(s3FolderContentReader);
                } else {
                    String parentDirectory = key.substring(0, key.lastIndexOf("/") + 1);
                    String name = key.substring(key.lastIndexOf("/") + 1);
                    String fileExtension = key.substring(key.lastIndexOf(".") + 1);
                    long fileSize = object.size();
                    long executionTime = (long) (System.nanoTime() - startFileTime) / 1_000_000;
                    FileInformation fileInfo = new FileInformation(name, lastModified, fileSize, parentDirectory, fileExtension, executionTime);
                    fileInfoNodes.add(fileInfo);
                }
            }
        }
        return fileInfoNodes;
    }
    
}
