package fr.ensai.scan.model.filesInformations;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotEmpty;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
abstract public class FileInformationNode {

    @NotEmpty(message = "File name missing")
    @Column(name = "file_name", insertable=false, updatable=false)
    private String name;

    @NotEmpty(message = "Date of modification missing")
    @Column(name = "file_date_modification")
    private String dateModification;

    @Column(name = "file_repository")
    private String repository;

    public FileInformationNode() {}

    public FileInformationNode(String name, String dateModification, String repository) {
        this.name = name;
        this.dateModification = dateModification;
        this.repository = repository;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDateModification() {
        return dateModification;
    }

    abstract public String getSize();

    public String getRepository() {
        return repository;
    }
    
}
