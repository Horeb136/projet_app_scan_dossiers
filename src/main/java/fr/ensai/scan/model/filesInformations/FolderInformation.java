package fr.ensai.scan.model.filesInformations;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import fr.ensai.scan.utils.DataFormatter;


public class FolderInformation extends FileInformationNode {

    private ArrayList<FileInformationNode> fileInformationNodes = new ArrayList<>();

    public FolderInformation() {}
    
    public FolderInformation(String name, String dateModification, String repository) {
        super(name, dateModification, repository);
    }

    @Override
    public String getSize() {
        long size = 0;
        for (FileInformationNode fileInformationNode: fileInformationNodes) {
            size += DataFormatter.deConvertSize(fileInformationNode.getSize());
        }
        return DataFormatter.convertSize(size);
    }

    public ArrayList<FileInformationNode> getFileInformationNodes() {
        return fileInformationNodes;
    }

    public void addFile(FileInformationNode fileInformationNode) {
        this.fileInformationNodes.add(fileInformationNode);
    }

    public List<FileInformationNode> browseFolder(File directory) {

        ArrayList<FileInformationNode> fileInfoNodes = new ArrayList<>();

        if (directory.isDirectory()) {
            File[] filesAndFolders = directory.listFiles();
 
            if (filesAndFolders != null) {
                for (File fileOrFolder : filesAndFolders) {
                    long startFileTime = System.nanoTime();
                    String fileName = fileOrFolder.getName().toString();
                    String directoryUp = fileOrFolder.getParent();

                    Date lastModifiedDate = new Date(fileOrFolder.lastModified());
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    String dateModif = dateFormat.format(lastModifiedDate);

                    if (fileOrFolder.isDirectory()) {
                        FolderInformation directoryNode = new FolderInformation(this.getName()+"/"+fileName, directoryUp, dateModif);
                        fileInfoNodes.add(directoryNode);
                    } else {
                        String fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                        long fileSize = fileOrFolder.length();
                        long executionTime = (long) (System.nanoTime() - startFileTime) / 1_000_000;
                        FileInformation fileInfo = new FileInformation(fileName, dateModif, fileSize, directoryUp, fileExtension, executionTime);
                        fileInfoNodes.add(fileInfo);
                    }
                }
            }
        } else {
            System.out.println("Le chemin spécifié ne correspond pas à un dossier.");
        }

        return fileInfoNodes;
    }

    @Override
    public String toString() {
        return "Name : " + this.getName() + "\n" +
                "Date Modif : " + this.getDateModification() + "\n" +
                "Repository : " + this.getRepository();
    }

}
