package fr.ensai.scan.model.filesInformations;

import com.fasterxml.jackson.annotation.JsonBackReference;

import fr.ensai.scan.model.Scan;
import fr.ensai.scan.utils.DataFormatter;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;

@Entity
@Table(name = "file_information")
public class FileInformation extends FileInformationNode {

    @EmbeddedId
    private FileInformationKey id;

    @NotEmpty(message = "File type missing")
    @Column(name = "file_type")
    private String fileType;

    @NotEmpty(message = "File Size missing")
    @Column(name = "file_size")
    private Long fileSize;

    @NotEmpty(message = "Execution time is missing")
    @Column(name = "execution_time")
    private Long executionTime;

    @ManyToOne
    @JoinColumn(name="scan_id", insertable = false, updatable = false)
    @JsonBackReference
    private Scan scan;

    public FileInformation() {
    }

    public FileInformation(String name, String dateModification, long fileSize, String repository, String fileType, Long executionTime) {
        super(name, dateModification, repository);
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.executionTime = executionTime;
    }

    public String getFileType() {
        return fileType;
    }

    @Override
    public String getSize() {
        return DataFormatter.convertSize(this.fileSize);
    }

    
    public void setScan(Scan scan) {
        this.scan = scan;
        this.id = new FileInformationKey(this.getName(), scan.getId());
    }

    public Scan getScan() {
        return scan;
    }

    public Long getScanId() {
        if (scan == null) return null;
        return scan.getId();
    }

	public String getExecutionTime() {
        return DataFormatter.convertExecTime(executionTime);
    }

    @Override
    public String toString() {
        String res = this.getDateModification() + " of size " + this.getSize() + " in " + this.getRepository();

        return "File : " + this.getName() + " modified " + res + " and of type of " + this.getFileType() + " with scan id " + this.getScanId();
    }

}
