package fr.ensai.scan.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import fr.ensai.scan.model.adapter.S3FolderAdapter;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.model.filesInformations.FileInformationNode;
import fr.ensai.scan.model.filesInformations.FolderInformation;
import fr.ensai.scan.model.filesInformations.S3FolderContentReader;
import fr.ensai.scan.utils.DataFormatter;

import java.io.File;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;

@Entity
@Table(name = "scans")
public class Scan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long scanId;

    @NotEmpty(message = "Repository missing")
    @Column(name = "repository")
    private String repository;

    @NotEmpty(message = "Max files missing")
    @Column(name = "max_files")
    private int maxFiles;

    @NotEmpty(message = "Max deph missing")
    @Column(name = "max_depth")
    private int maxDepth;

    @Column(name = "file_filter")
    private String fileFilter;

    @Column(name = "type_filter")
    private String typeFilter;

    @NotEmpty(message = "Scan date missing")
    @Column(name = "scan_date")
    private String scanDate;

    @NotEmpty(message = "Execution time missing")
    @Column(name = "execution_time")
    private Long executionTime;

    @Column(name = "mean_folder_execution_time")
    private Long meanFolderExecutionTime;

    @OneToMany(mappedBy = "scan", cascade = CascadeType.DETACH)
    private List<FileInformation> files = new ArrayList<>();

    @Column(name = "bucket_name")
    private String bucketName;

    public Scan() {
        setScanDate();
    }
    
    public Scan(long scanId, String repository, int maxFiles, int maxDepth, String fileFilter, String typeFilter){
        this.scanId = scanId;
        this.repository = repository;
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        this.fileFilter = fileFilter;
        this.typeFilter = typeFilter;
        setScanDate();
    }

    public Scan(long scanId, String bucketName, String repository, int maxFiles, int maxDepth, String fileFilter, String typeFilter){
        this.scanId = scanId;
        this.bucketName = bucketName;
        this.repository = repository;
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        this.fileFilter = fileFilter;
        this.typeFilter = typeFilter;
        setScanDate();
    }

    public Scan(String repository, int maxFiles, int maxDepth, String fileFilter, String typeFilter){
        this.repository = repository;
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        this.fileFilter = fileFilter;
        this.typeFilter = typeFilter;
        setScanDate();
    }

    public Scan(String bucketName, String repository, int maxFiles, int maxDepth, String fileFilter, String typeFilter){
        this.bucketName = bucketName;
        this.repository = repository;
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        this.fileFilter = fileFilter;
        this.typeFilter = typeFilter;
        setScanDate();
    }

    public Scan(String repository, int maxFiles, int maxDepth){
        this.repository = repository;
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        setScanDate();
    }

    public Long getId() {
        return scanId;
    }

    public int getMaxFiles() {
        return maxFiles;
    }

    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public String getFileFilter() {
        return fileFilter;
    }

    public void setFileFilter(String fileFilter) {
        this.fileFilter = fileFilter;
    }

    public String getTypeFilter() {
        return typeFilter;
    }

    public void setTypeFilter(String typeFilter) {
        this.typeFilter = typeFilter;
    }

    public String getScanDate() {
        return scanDate;
    }

    private void setScanDate() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        this.scanDate = date.format(formatter);
	}

    public String getExecutionTime() {
        return DataFormatter.convertExecTime(executionTime);
    }

    public String getMeanFolderExecutionTime() {
        return DataFormatter.convertExecTime(meanFolderExecutionTime);
    }

    public String getRepository() {
        return repository;
    }

    public String getBucketName() {
        return bucketName;
    }

    public List<FileInformationNode> browseFolder(FolderInformation folderInformation) {
        List<FileInformationNode> fileInfoNodes = new ArrayList<>();

        File directory = new File(folderInformation.getName());
        List<FileInformationNode> docs = folderInformation.browseFolder(directory);
        for (FileInformationNode fileInfoNode : docs) {
            if (fileInfoNode instanceof FolderInformation){
                fileInfoNodes.add(fileInfoNode);
            } else {
                FileInformation fileInformation = (FileInformation) fileInfoNode;
                fileInformation.setScan(this);
                String filename = fileInformation.getName();
                int lastDotIndex = filename.lastIndexOf('.');
                String fileNameWithoutExtension = lastDotIndex > 0 ? filename.substring(0, lastDotIndex) : filename;
                if (this.typeFilter != null) { // Vérification de l'existence d'un filtre sur le type de fichier
                    if (this.fileFilter != null) { // Vérification de l'existence d'un filtre sur le nom du fichier
                        if (fileInformation.getFileType().equals(this.typeFilter) &
                        fileNameWithoutExtension.equals(this.fileFilter)) {
                            this.files.add(fileInformation);
                        }
                    } else {
                        if (fileInformation.getFileType().equals(this.typeFilter)) {
                            this.files.add(fileInformation);
                        }
                    }
                } else {
                    if (this.fileFilter != null) { // Vérification de l'existence d'un filtre sur le nom de fichier
                        if (fileNameWithoutExtension.equals(this.fileFilter)) {
                            this.files.add(fileInformation);
                        }
                    } else {
                        this.files.add(fileInformation);
                    }
                }
            }
            if (this.maxFiles == this.files.size()) break;
        }
        return fileInfoNodes;
    }

    public void scanner() {
        long startTime = System.nanoTime();
        List<Long> listFolderTime = new ArrayList<>();
        long startFolderTime = System.nanoTime();

        List<FileInformationNode> docs = new ArrayList<>();

        S3Client s3Client = S3Client.builder()
                .region(Region.US_EAST_1)
                .build();

        if (this.bucketName == null) {
            docs = this.browseFolder(new FolderInformation(this.repository, null, null));
            s3Client.close();
        } else {

            ListObjectsV2Request listObjectsRequest;
            if (this.repository.equals("/")) {
                listObjectsRequest = ListObjectsV2Request.builder()
                    .bucket(this.bucketName)
                    .build();
            } else {
                listObjectsRequest = ListObjectsV2Request.builder()
                    .bucket(this.bucketName)
                    .prefix(this.repository)
                    .build();
            }

            S3FolderContentReader s3FolderContentReader = new S3FolderContentReader(this.repository, null, null, this.bucketName);
            S3FolderAdapter s3FolderAdapter = new S3FolderAdapter(s3FolderContentReader);
            docs = s3FolderAdapter.browseFolder(this, s3Client, listObjectsRequest);
        }
        
        listFolderTime.add((System.nanoTime() - startFolderTime) / 1_000_000);
        int i = 0;
        while ((i < this.maxDepth) && (this.files.size() < this.maxFiles)) {
            List<FileInformationNode> docs1 = new ArrayList<>();
            for (FileInformationNode fileInfoNode: docs) {
                startFolderTime = System.nanoTime();
                if (this.bucketName != null) {

                    S3FolderContentReader s3FolderContentReader = (S3FolderContentReader) fileInfoNode;
                    S3FolderAdapter s3FolderAdapter = new S3FolderAdapter(s3FolderContentReader);

                    ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
                        .bucket(s3FolderContentReader.getBucketName())
                        .prefix(s3FolderContentReader.getName())
                        .build();
                    
                    docs1.addAll(s3FolderAdapter.browseFolder(this, s3Client, listObjectsRequest));
                } else {
                    docs1.addAll(this.browseFolder((FolderInformation) fileInfoNode));
                }
                listFolderTime.add((System.nanoTime() - startFolderTime) / 1_000_000);
                if (this.files.size() == this.maxFiles) break;
            }
            i++;
            if (docs1.size() == 0) break;
            docs = docs1;
            
        }
        s3Client.close();
        this.executionTime = (System.nanoTime() - startTime) / 1_000_000;
        this.meanFolderExecutionTime = (long) listFolderTime.stream().mapToLong(Long::longValue).average().orElse(0.0);
    }

    public boolean compareScans(Scan scan) {
        if (!(this.repository.equals(scan.getRepository()))) {
            return false;
        }
        if ((this.bucketName != null) && (scan.getBucketName() != null)) {
            if (!(this.bucketName.equals(scan.getBucketName()))) {
                return false;
            }
        }
        if ((this.bucketName != null) && (scan.getBucketName() == null)) {
            return false;
        }
        if ((this.bucketName == null) && (scan.getBucketName() != null)) {
            return false;
        }
        if (this.maxFiles != scan.getMaxFiles()) {
            return false;
        }
        if (this.maxDepth != scan.getMaxDepth()) {
            return false;
        }
        if ((this.fileFilter != null) && (scan.getFileFilter() != null)) {
            if (!(this.fileFilter.equals(scan.getFileFilter()))) {
                return false;
            }
        }
        if ((this.fileFilter != null) && (scan.getFileFilter() == null)) {
            return false;
        }
        if ((this.fileFilter == null) && (scan.getFileFilter() != null)) {
            return false;
        }
        if ((this.typeFilter != null) && (scan.getTypeFilter() != null)) {
            if (!(this.typeFilter.equals(scan.getTypeFilter()))) {
                return false;
            }
        }
        if ((this.typeFilter != null) && (scan.getTypeFilter() == null)) {
            return false;
        }
        if ((this.typeFilter == null) && (scan.getTypeFilter() != null)) {
            return false;
        }
        return true;
    }

    public List<FileInformation> getFiles() {
        return files;
    }

    public void addFile(FileInformation fileInformation) {
        this.files.add(fileInformation);
    }

    @Override
    public String toString() {

        String res = "";
        res += "Scan d'identifiant : " + this.scanId + "\n";
        res += "Dossier : " + this.repository + "\n";
        res += "Max files : " + this.maxFiles + "\n";
        res += "Max depth : " + this.maxDepth + "\n";
        res += "File filter : " + this.fileFilter + "\n";
        res += "Type filter : " + this.typeFilter + "\n";
        res += "Date du scan " + this.scanDate + "\n";
        res += "Fichiers : \n";
        for (FileInformation fileInformation : this.files) {
            res += fileInformation.toString() + "\n";
        }
        res += "Temps mis : " + this.executionTime;
        return res;
    }
    
}
