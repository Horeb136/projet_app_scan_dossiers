package fr.ensai.scan.model.adapter;

import java.util.ArrayList;
import java.util.List;

import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.model.filesInformations.FileInformationNode;
import fr.ensai.scan.model.filesInformations.S3FolderContentReader;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;

public class S3FolderAdapter {
    private S3FolderContentReader s3FolderContentReader;

    public S3FolderAdapter (S3FolderContentReader s3FolderContentReader) {
        this.s3FolderContentReader = s3FolderContentReader;
    }

    public S3FolderContentReader getS3FolderContentReader() {
        return s3FolderContentReader;
    }

    public List<FileInformationNode> browseFolder(Scan scan, S3Client s3Client, ListObjectsV2Request listObjectsRequest) {
        
        List<FileInformationNode> fileInfoNodes = new ArrayList<>();
        
        for (FileInformationNode fileInfoNode : this.s3FolderContentReader.browseFolder(s3Client, listObjectsRequest)) {
            if (fileInfoNode instanceof S3FolderContentReader) {
                fileInfoNodes.add(fileInfoNode);
            } else {
                FileInformation fileInformation = (FileInformation) fileInfoNode;
                fileInformation.setScan(scan);
                String filename = fileInformation.getName();
                int lastDotIndex = filename.lastIndexOf('.');
                String fileNameWithoutExtension = lastDotIndex > 0 ? filename.substring(0, lastDotIndex) : filename;
                if (scan.getTypeFilter() != null) {
                    if (scan.getFileFilter() != null) {
                        if (fileInformation.getFileType().equals(scan.getTypeFilter()) &
                        fileNameWithoutExtension.equals(scan.getFileFilter())) {
                            scan.addFile(fileInformation);
                        }
                    } else {
                        if (fileInformation.getFileType().equals(scan.getTypeFilter())) {
                            scan.addFile(fileInformation);
                        }
                    }
                } else {
                    if (scan.getFileFilter() != null) {
                        if (fileNameWithoutExtension.equals(scan.getFileFilter())) {
                            scan.addFile(fileInformation);
                        }
                    } else {
                        scan.addFile(fileInformation);
                    }
                }
            }
            if (scan.getMaxFiles() == scan.getFiles().size()) break;
        }
        return fileInfoNodes;
    }

}
