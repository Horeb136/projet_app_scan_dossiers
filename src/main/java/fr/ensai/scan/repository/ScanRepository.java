package fr.ensai.scan.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.scan.model.Scan;

@Repository
public interface ScanRepository extends CrudRepository<Scan, Long> {
}
