package fr.ensai.scan.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.model.filesInformations.FileInformationKey;

@Repository
public interface FileInformationRepository extends CrudRepository<FileInformation, FileInformationKey> {
    public Iterable<FileInformation> findByScan_id(Long scan_id);
    public Iterable<FileInformation> findByName(String name);
    public Iterable<FileInformation> findByFileType(String fileType);
}
