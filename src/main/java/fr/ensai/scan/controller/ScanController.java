package fr.ensai.scan.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.ensai.scan.dto.SearchInDto;
import fr.ensai.scan.model.Scan;
import fr.ensai.scan.service.ScanService;

@RestController
@RequestMapping("/scan")
public class ScanController {

    Logger LOG  = LoggerFactory.getLogger(ScanController.class);

    @Autowired
    private ScanService scanService;

    @GetMapping("/")
    public Iterable<Scan> getAllScan() {
        return scanService.getAllScan();
    }

    @GetMapping("/count")
    public long countScans() {
        return scanService.countScans();
    }

    @GetMapping("/{id}")
    public Optional<Scan> getScanById(@PathVariable Long id) {
        return scanService.getScanById(id);
    }

    @PostMapping("/create")
    public ResponseEntity<String> createScan(@RequestBody Scan scan) {
        boolean created = scanService.createScan(scan);
        if (!created) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>("Scan created\n", HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable Long id) {
        Optional<Scan> scanOptional = scanService.getScanById(id);
        if (scanOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        boolean deleted = scanService.deleteScanById(id);
        if (!deleted) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>("Scan " + id + " deleted\n", HttpStatus.OK);
    }

    @PostMapping("/duplicate/{id}")
    public ResponseEntity<String> duplicateScan(@RequestBody SearchInDto searchInDto, @PathVariable Long id) {
        Optional<Scan> scanOptional = scanService.getScanById(id);
        if (scanOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        boolean duplicated = scanService.duplicateScan(id, searchInDto);
        if (!duplicated) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<String>("Scan duplicated\n", HttpStatus.OK);
    }

    @PostMapping("/replay/{id}")
    public ResponseEntity<String> replayScan(@PathVariable long id) {
        Optional<Scan> scanOptional = scanService.getScanById(id);
        if (scanOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        
        // Scan trouvé, maintenant on peut le rejouer
        boolean replayed = scanService.replayScan(id);
        if (!replayed) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        return new ResponseEntity<String>("Scan " + id + " replayed\n", HttpStatus.OK);
    }

    @GetMapping("/compare/{id1}/{id2}")
    public boolean compareScans(@PathVariable long id1, @PathVariable long id2) {
        boolean result = scanService.compareScans(id1, id2);
        return result;
    }

    @GetMapping("/statistics/{id}")
    public Iterable<String> getScansStatistics(@PathVariable long id) {
        return scanService.getScansStatistics(id);
    }

}
