package fr.ensai.scan.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.service.FileInformationService;

@RestController
@RequestMapping("/files")
public class FileInformationController {

    Logger LOG  = LoggerFactory.getLogger(FileInformationController.class);

    @Autowired
    private FileInformationService fileInformationService;

    @GetMapping("/")
    public Iterable<FileInformation> getAllFileInformation() {
        return fileInformationService.getAllFileInformation();
    }

    @GetMapping("/scan/{id}")
    public Iterable<FileInformation> getFileInformationByScanId(@PathVariable Long id) {
        return fileInformationService.getFileInformationByScanId(id);
    }

    @GetMapping("/count")
    public long countFileInformation() {
        return fileInformationService.countFileInformation();
    }

    @GetMapping("/name/{name}")
    public Iterable<FileInformation> getFileInformationByName(@PathVariable String name) {
        return fileInformationService.getFileInformationByName(name);
    }

    @GetMapping("/type/{fileType}")
    public Iterable<FileInformation> getFileInformationByType(@PathVariable String fileType) {
        return fileInformationService.getFileInformationByType(fileType);
    }

}
