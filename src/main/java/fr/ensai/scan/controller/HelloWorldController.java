package fr.ensai.scan.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    @GetMapping("/")
    public String helloWorld() {
        return "<html>\n"+
                "<head><title> Ma page d'essai </title></head>"+
                "<body>"+
                "<div style=\"text-align: center;\">"+
                "<h1> DevOps Project Scan repository </h1>"+
                "<p> Création et déploiement d’une application web backend qui scanne les répertoires et\n" + //
                                        "les fichiers </p>"+
                "</div></body></html>";
    }
}
