package fr.ensai.scan.dto;

public class SearchInDto {

    private int maxFiles;
    private int maxDepth;
    private String typeFilter;
    private String fileFilter;

    public SearchInDto(int maxFiles, int maxDepth, String typeFilter, String fileFilter) {
        this.maxFiles = maxFiles;
        this.maxDepth = maxDepth;
        this.typeFilter = typeFilter;
        this.fileFilter = fileFilter;
    }
    
    public int getMaxFiles() {
        return maxFiles;
    }
    public int getMaxDepth() {
        return maxDepth;
    }
    public String getTypeFilter() {
        return typeFilter;
    }
    public String getFileFilter() {
        return fileFilter;
    }
    
}
