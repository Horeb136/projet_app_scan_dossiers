package fr.ensai.scan.service;


import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.model.filesInformations.FileInformationKey;
import fr.ensai.scan.repository.FileInformationRepository;
import fr.ensai.scan.utils.DataFormatter;

@Service
public class FileInformationService {

  Logger LOG  = LoggerFactory.getLogger(FileInformationService.class);
  
  @Autowired
  private FileInformationRepository fileInformationRepository;

  public Iterable<FileInformation> getAllFileInformation() {
    return fileInformationRepository.findAll();
  }
  
  public Iterable<FileInformation> getFileInformationByScanId(final Long id) {
    return fileInformationRepository.findByScan_id(id);
  }

  public Iterable<FileInformation> getFileInformationByName(final String name) {
    return fileInformationRepository.findByName(name);
  }

  public Iterable<FileInformation> getFileInformationByType(final String type) {
    return fileInformationRepository.findByFileType(type);
  }

  public boolean createFileInformation(FileInformation file) {
    FileInformation fileSaved = fileInformationRepository.save(file);
    Optional<FileInformation> fileOptional = Optional.ofNullable(fileSaved);
    if (fileOptional.isPresent()) {
      return true;
    }
    return false;
  }

  public boolean createFileInformations(Iterable<FileInformation> files) {
    Iterable<FileInformation> filesSaved = fileInformationRepository.saveAll(files);
    Optional<Iterable<FileInformation>> fileOptional = Optional.ofNullable(filesSaved);
    if (fileOptional.isPresent()) {
      return true;
    }
    return false;
  }

  public long countFileInformation() {
    return fileInformationRepository.count();
  }


  public boolean deleteFileInformationByScanId(final Long id) {
    Iterable<FileInformation> filesOptional = getFileInformationByScanId(id);
    if (filesOptional != null && filesOptional.iterator().hasNext()) {
        fileInformationRepository.deleteAll(filesOptional);

        ArrayList<FileInformationKey> fileInformationKeys = new ArrayList<>();
        for (FileInformation fileInformation : filesOptional) {
            FileInformationKey fileInformationKey = new FileInformationKey(fileInformation.getName(), fileInformation.getScanId());
            fileInformationKeys.add(fileInformationKey);
        }
        Iterable<FileInformationKey> iterableFileInformationKeys = fileInformationKeys;
        Iterable<FileInformation> files = fileInformationRepository.findAllById(iterableFileInformationKeys); // getFileInformationByScanId(id);
        return !files.iterator().hasNext();
    } else {
      if (filesOptional != null) {
        return true;
      }
    }
    return false;
  }

  public double meanFileExecutionTime(final Long id) {
    Iterable<FileInformation> filesOptional = getFileInformationByScanId(id);
    if (filesOptional != null && filesOptional.iterator().hasNext()) {
      long somme = 0;
      long count = 0;
      for (FileInformation fileInformation : filesOptional) {
        somme += DataFormatter.convertStringToDuration(fileInformation.getExecutionTime());
        count++;
      }
      return somme / count;
    }
    return 0.0;
  }

}