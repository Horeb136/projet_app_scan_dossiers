package fr.ensai.scan.service;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ensai.scan.dto.SearchInDto;
import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.repository.ScanRepository;
import fr.ensai.scan.utils.DataFormatter;

@Service
public class ScanService {

  Logger LOG  = LoggerFactory.getLogger(ScanService.class);
  
  @Autowired
  private ScanRepository scanRepository;
  @Autowired
  private FileInformationService fileInformationService;

  public Optional<Scan> getScanById(Long id) {
    return scanRepository.findById(id);
  }

  public Iterable<Scan> getAllScan() {
    return scanRepository.findAll();
  }


  public boolean deleteScanById(Long id) {
    boolean fileDeleted = fileInformationService.deleteFileInformationByScanId(id);
    if (fileDeleted) {
      scanRepository.deleteById(id);
      return !scanRepository.existsById(id);
    }
    return false;
  }

  public long countScans() {
    return scanRepository.count();
  }

  public boolean createScan(Scan scan) {
    
    scan.scanner();
    
    Scan savedScan = scanRepository.save(scan);
    Optional<Scan> scanOptional = Optional.ofNullable(savedScan);
    if (scanOptional.isPresent()) {
      for (FileInformation file: savedScan.getFiles()) {
        file.setScan(savedScan);
        boolean filecreated = fileInformationService.createFileInformation(file);
        if (!filecreated) {
          System.out.println("File not created");
        }
      }
      return true;
    }
    return false;
  }

  public boolean duplicateScan(Long id, SearchInDto searchInDto) {
    Optional<Scan> optScan = scanRepository.findById(id);
    if (optScan.isPresent()) {
      Scan scan = optScan.get();
      Scan new_scan = new Scan(scan.getBucketName(), scan.getRepository(), searchInDto.getMaxFiles(), searchInDto.getMaxDepth(), searchInDto.getFileFilter(), searchInDto.getTypeFilter());
      boolean scanCreated = createScan(new_scan);
      return scanCreated;
    }
    return false;
  }

  public boolean replayScan(long id) {
    Optional<Scan> optScan = scanRepository.findById(id);
    if (optScan.isPresent()) {
      Scan scan = optScan.get();
      Scan new_scan = new Scan(scan.getBucketName(), scan.getRepository(), scan.getMaxFiles(), scan.getMaxDepth(), scan.getFileFilter(), scan.getTypeFilter());
      boolean scanCreated = createScan(new_scan);
      return scanCreated;
    }
    return false;
  }

  public boolean compareScans(long id1, long id2) {
    Optional<Scan> optScan1 = scanRepository.findById(id1);
    Optional<Scan> optScan2 = scanRepository.findById(id2);
    
    if (optScan1.isPresent() && optScan2.isPresent()) {
      Scan scan1 = optScan1.get();
      Scan scan2 = optScan2.get();
      return scan1.compareScans(scan2);
    }
    return false;
  }

  public Iterable<String> getScansStatistics(long id) {
    Iterable<String> res = new ArrayList<>();
    Optional<Scan> optScan = scanRepository.findById(id);
    if (optScan.isPresent()) {
      Scan scan = optScan.get();
      ((ArrayList<String>) res).add("Mean folder execution time : " + scan.getMeanFolderExecutionTime());
      String meanFile =  DataFormatter.convertDurationToString(fileInformationService.meanFileExecutionTime(id));
      ((ArrayList<String>) res).add("Mean file execution time : " + meanFile);
      return res;
    }
    return null;
  }

}