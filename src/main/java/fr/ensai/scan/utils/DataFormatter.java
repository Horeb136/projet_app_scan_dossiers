package fr.ensai.scan.utils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.context.annotation.Configuration;

@Configuration
public class DataFormatter {
    public static String convertSize(Long size) {
        if (size == null) return "0 octet";
        String res;
        if (size >= 1024 * 1024 * 1024) { // Si la taille est supérieure à 1 Go
            res = String.format(Locale.ENGLISH, "%.1f", size / (1024.0 * 1024 * 1024)) + " Go";
        } else if (size >= 1024 * 1024) { // Si la taille est supérieure à 1 Mo
            res = String.format(Locale.ENGLISH, "%.1f", size / (1024.0 * 1024)) + " Mo";
        } else if (size >= 1024) { // Si la taille est supérieure à 1 Ko
            res = String.format(Locale.ENGLISH, "%.1f", size / 1024.0) + " Ko";
        } else {
            res = size + " octets";
        }
        return res;
    }

    public static Long deConvertSize(String size) {
        Double res;
        String[] convertedSizeArray = size.split(" ");

        String sizeUnit = convertedSizeArray[1].toLowerCase();
        double sizeValue = Double.parseDouble(convertedSizeArray[0].trim());

        if (sizeUnit.equals("go")) {
            res = sizeValue * Math.pow(1024, 3);
        } else if (sizeUnit.equals("mo")) {
            res = sizeValue * Math.pow(1024, 2);
        } else if (sizeUnit.equals("ko")) {
            res = sizeValue * 1024;
        } else {
            res = sizeValue;
        }
        
        return res.longValue();
    }
    
    public static String convertExecTime(Long milliseconds) {
        if (milliseconds == null) {
            return null;
        }
        long seconds = milliseconds / 1000;
        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        long remainingSeconds = seconds % 60;
        long remainingMilliseconds = milliseconds % 1000;
    
        if (hours > 0) {
            return String.format("%dh%02dm%02ds%03dms", hours, minutes, remainingSeconds, remainingMilliseconds);
        } else if (minutes > 0) {
            return String.format("%dm%02ds%03dms", minutes, remainingSeconds, remainingMilliseconds);
        } else if (remainingSeconds > 0) {
            return String.format("%ds%03dms", remainingSeconds, remainingMilliseconds);
        } else {
            return String.format("%dms", remainingMilliseconds);
        }
    }

    public static Long convertStringToDuration(String timeString) {
        if (timeString == null) {
            return null;
        }
    
        Pattern pattern = Pattern.compile("(\\d+)h(\\d{2})m(\\d{2})s(\\d{3})ms");
        Matcher matcher = pattern.matcher(timeString);
        if (matcher.matches()) {
            long hours = Long.parseLong(matcher.group(1));
            long minutes = Long.parseLong(matcher.group(2));
            long seconds = Long.parseLong(matcher.group(3));
            long milliseconds = Long.parseLong(matcher.group(4));
            return hours * 3600 * 1000 + minutes * 60 * 1000 + seconds * 1000 + milliseconds;
        } else {
            pattern = Pattern.compile("(\\d+)m(\\d{2})s(\\d{3})ms");
            matcher = pattern.matcher(timeString);
            if (matcher.matches()) {
                long minutes = Long.parseLong(matcher.group(1));
                long seconds = Long.parseLong(matcher.group(2));
                long milliseconds = Long.parseLong(matcher.group(3));
                return minutes * 60 * 1000 + seconds * 1000 + milliseconds;
            } else {
                pattern = Pattern.compile("(\\d+)s(\\d{3})ms");
                matcher = pattern.matcher(timeString);
                if (matcher.matches()) {
                    long seconds = Long.parseLong(matcher.group(1));
                    long milliseconds = Long.parseLong(matcher.group(2));
                    return seconds * 1000 + milliseconds;
                } else {
                    pattern = Pattern.compile("(\\d+)ms");
                    matcher = pattern.matcher(timeString);
                    if (matcher.matches()) {
                        return Long.parseLong(matcher.group(1));
                    } else {
                        throw new IllegalArgumentException("Invalid time format.");
                    }
                }
            }
        }
    }

    public static String convertDurationToString(Double duration) {
        if (duration == null) {
            return null;
        }
        
        long totalSeconds = (long) (duration / 1_000);
        long hoursResult = totalSeconds / 3_600;
        long minutesResult = (totalSeconds % 3_600) / 60;
        long remainingSeconds = totalSeconds % 60;
        long milliseconds = (long) (duration / 1) % 1_000 ;
    
        if (hoursResult > 0) {
            return String.format("%dh%02dm%02ds%03dms", hoursResult, minutesResult, remainingSeconds, milliseconds);
        } else if (minutesResult > 0) {
            return String.format("%dm%02ds%03dms", minutesResult, remainingSeconds, milliseconds);
        } else if (remainingSeconds > 0) {
            return String.format("%ds%03dms", remainingSeconds, milliseconds);
        } else {
            return String.format("%dms", milliseconds);
        }
    }
}
