package fr.ensai.scan.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class SearchInDtoTest {

    private SearchInDto searchInDto;

    @BeforeAll
    public void setUp() {
        searchInDto = new SearchInDto(100, 5, "java", "code");
    }
    
    @Test
    public void testGetMaxFiles() {
        assertEquals(100, searchInDto.getMaxFiles());
    }

    @Test
    public void testGetMaxDepth() {
        assertEquals(5, searchInDto.getMaxDepth());
    }

    @Test
    public void testGetTypeFilter() {
        assertEquals("java", searchInDto.getTypeFilter());
    }

    @Test
    public void testGetFileFilter() {
        assertEquals("code", searchInDto.getFileFilter());
    }
}
