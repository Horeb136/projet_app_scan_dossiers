package fr.ensai.scan.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.model.filesInformations.FileInformationNode;
import fr.ensai.scan.model.filesInformations.FolderInformation;

@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class ScanTest {

    @MockBean
    private FolderInformation folderInformation;

    private Scan scan1;
    private Scan scan2;
    private Scan scan3;
    private Scan scan4;
    private Scan scan5;

    @BeforeEach
    public void setUp() {
        // Given
        scan1 = new Scan("testRepository", 10, 2, "code", "java");
        scan2 = new Scan(Long.valueOf(1), "testRepository", 10, 2, "code", "java");
        scan3 = new Scan("testBucketName", "testRepository", 10, 2, "data", "txt");
        scan4 = new Scan(Long.valueOf(2), "myBucketName", "testRepository", 10, 2, "data", "txt");
        scan5 = new Scan("myRepository", 10, 2);
    }

    @Test
    public void testGetId() {
        assertNull(scan1.getId());
        assertEquals(Long.valueOf(1), scan2.getId());
        assertNull(scan3.getId());
        assertEquals(Long.valueOf(2), scan4.getId());
    }

    @Test
    public void testGetMaxFiles() {
        // Then
        assertEquals(10, scan1.getMaxFiles());
    }

    @Test
    public void testSetMaxFiles() {
        scan1.setMaxFiles(20);
        assertEquals(20, scan1.getMaxFiles());
    }

    @Test
    public void testGetMaxDepth() {
        assertEquals(2, scan1.getMaxDepth());
    }

    @Test
    public void testSetMaxDepth() {
        scan1.setMaxDepth(3);
        assertEquals(3, scan1.getMaxDepth());
    }

    @Test
    public void testGetFileFilter() {
        assertEquals("code", scan1.getFileFilter());
        assertNull(scan5.getFileFilter());
    }

    @Test
    public void testSetFileFilter() {
        scan1.setFileFilter("essai");
        assertEquals("essai", scan1.getFileFilter());
    }

    @Test
    public void testGetTypeFilter() {
        assertEquals("java", scan1.getTypeFilter());
        assertNull(scan5.getTypeFilter());
    }

    @Test
    public void testSetTypeFilter() {
        scan1.setTypeFilter("csv");
        assertEquals("csv", scan1.getTypeFilter());
    }

    @Test
    public void testGetScanDate() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        assertEquals(date.format(formatter), scan1.getScanDate());
    }

    @Test
    public void testGetRepository() {
        assertEquals("testRepository", scan1.getRepository());
    }

    @Test
    public void testGetBucketName() {
        assertNull(scan1.getBucketName());
        assertEquals("testBucketName", scan3.getBucketName());
    }

    @Test
    public void testCompareScans() {

        Scan scan6 = new Scan("testRepository", 8, 2, "code", "txt");
        Scan scan7 = new Scan("testRepository", 10, 4, "code", "txt");
        Scan scan8 = new Scan("testRepository", 10, 2, "data", "txt");
        Scan scan9 = new Scan("testRepository", 10, 2, null, "txt");
        Scan scan10 = new Scan("testRepository", 10, 2, "code", null);
        Scan scan11 = new Scan("testRepository", 10, 2, "code", "csv");

        assertFalse(scan1.compareScans(scan5)); // different repository
        assertFalse(scan1.compareScans(scan3)); // One has no bucket
        assertFalse(scan3.compareScans(scan1));
        assertFalse(scan3.compareScans(scan4)); // different bucket
        assertFalse(scan1.compareScans(scan6)); // different maxFiles
        assertFalse(scan1.compareScans(scan7)); // different maxDepth
        assertFalse(scan1.compareScans(scan8));  // different fileFilter
        assertFalse(scan1.compareScans(scan9)); // One has no fileFilter
        assertFalse(scan9.compareScans(scan1));
        assertFalse(scan1.compareScans(scan10)); // One has no typeFilter
        assertFalse(scan10.compareScans(scan1));
        assertFalse(scan1.compareScans(scan11)); // different typeFilter

        assertTrue(scan1.compareScans(scan1));
        assertTrue(scan1.compareScans(scan2));
        assertTrue(scan3.compareScans(scan3));
        assertTrue(scan5.compareScans(scan5));
        assertTrue(scan9.compareScans(scan9));
        assertTrue(scan10.compareScans(scan10));
    }

    @Test
    public void testToString() {
        // When
        FileInformation fileInformation = new FileInformation();
        scan2.addFile(fileInformation);
        // Then
        String res = scan2.toString();
        assertTrue(res.contains("Scan d'identifiant : 1\nDossier : testRepository"));
    }

    @Test
    public void testBrowseFolder() {
        
        FolderInformation folderInformation = Mockito.mock(FolderInformation.class);

        FolderInformation subFolder = new FolderInformation(); //Mockito.mock(FolderInformation.class);
        FileInformation file1 = new FileInformation("code.java", "20/10/2023", 0, "testRepository", "java", null);
        FileInformation file2 = new FileInformation("metadata.java", "20/10/2023", 0, "testRepository", "java", null);
        FileInformation file3 = new FileInformation("metadata", "20/10/2023", 0, "testRepository", "metadata", null);

        List<FileInformationNode> listFiles = new ArrayList<>();
        listFiles.add(file1);
        listFiles.add(subFolder);
        listFiles.add(file2);
        listFiles.add(file3);
        
        Mockito.when(folderInformation.getName()).thenReturn("testRepository");
        Mockito.when(folderInformation.browseFolder(new File("testRepository"))).thenReturn(listFiles);

        List<FileInformationNode> files = scan1.browseFolder(folderInformation);
        assertFalse(files.isEmpty());
        assertEquals(1, files.size());
        assertEquals(1, scan1.getFiles().size());

        files = scan5.browseFolder(folderInformation);
        assertFalse(files.isEmpty());
        assertEquals(1, files.size());
        assertEquals(3, scan5.getFiles().size());

        Scan scan6 = new Scan("testRepository", 10, 2, null, "java");
        files = scan6.browseFolder(folderInformation);
        assertFalse(files.isEmpty());
        assertEquals(1, files.size());
        assertEquals(2, scan6.getFiles().size());

        Scan scan7 = new Scan("testRepository", 10, 2, "metadata", null);
        files = scan7.browseFolder(folderInformation);
        assertFalse(files.isEmpty());
        assertEquals(1, files.size());
        assertEquals(2, scan7.getFiles().size());

        Scan scan8 = new Scan("testRepository", 2, 2);
        files = scan8.browseFolder(folderInformation);
        assertFalse(files.isEmpty());
        assertEquals(1, files.size());
        assertEquals(2, scan8.getFiles().size());
    }

    @Test
    public void testScanner() {

        MockitoAnnotations.openMocks(this);

        FolderInformation folderInformation1 = Mockito.mock(FolderInformation.class);
        FolderInformation folderInformation2 = Mockito.mock(FolderInformation.class);
        FileInformation file1 = new FileInformation("code.java", "20/10/2023", 0, "testRepository", "java", null);
        FileInformation file2 = new FileInformation("metadata.java", "20/10/2023", 0, "testRepository", "java", null);
        FileInformation file3 = new FileInformation("metadata", "20/10/2023", 0, "testRepository", "metadata", null);
        List<FileInformationNode> listFiles1 = new ArrayList<>();
        listFiles1.add(file1);
        listFiles1.add(folderInformation1);
        listFiles1.add(file2);
        listFiles1.add(folderInformation2);
        listFiles1.add(file3);

        FolderInformation myClass = new FolderInformation("myRepository", null, null);
        FolderInformation folderInformationSpy = Mockito.spy(myClass);
        Mockito.doReturn(listFiles1).when(folderInformationSpy).browseFolder(any(File.class));
        
        FolderInformation folderInformation3 = Mockito.mock(FolderInformation.class);
        FileInformation file4 = new FileInformation("data.csv", "20/10/2023", 0, "subTestRepository1", "csv", null);
        FileInformation file5 = new FileInformation("metadata.txt", "20/10/2023", 0, "subTestRepository1", "txt", null);
        List<FileInformationNode> listFiles2 = new ArrayList<>();
        listFiles2.add(file4);
        listFiles2.add(folderInformation3);
        listFiles2.add(file5);
        Mockito.when(folderInformation1.getName()).thenReturn("subTestRepository1");
        Mockito.when(folderInformation1.browseFolder(new File("subTestRepository1"))).thenReturn(listFiles2);

        FolderInformation folderInformation4 = Mockito.mock(FolderInformation.class);
        FileInformation file6 = new FileInformation("code.py", "20/10/2023", 0, "subTestRepository2", "py", null);
        FileInformation file7 = new FileInformation("essai.py", "20/10/2023", 0, "subTestRepository2", "py", null);
        List<FileInformationNode> listFiles3 = new ArrayList<>();
        listFiles3.add(file6);
        listFiles3.add(folderInformation4);
        listFiles3.add(file7);
        Mockito.when(folderInformation2.getName()).thenReturn("subTestRepository2");
        Mockito.when(folderInformation2.browseFolder(new File("subTestRepository2"))).thenReturn(listFiles3);

        FolderInformation folderInformation5 = Mockito.mock(FolderInformation.class);
        FileInformation file8 = new FileInformation("essai", "20/10/2023", 0, "repository1", "essai", null);
        List<FileInformationNode> listFiles4 = new ArrayList<>();
        listFiles4.add(file8);
        listFiles4.add(folderInformation5);
        Mockito.when(folderInformation3.getName()).thenReturn("repository1");
        Mockito.when(folderInformation3.browseFolder(new File("repository1"))).thenReturn(listFiles4);

        FileInformation file9 = new FileInformation("data.txt", "20/10/2023", 0, "repository2", "txt", null);
        FileInformation file10 = new FileInformation("code.r", "20/10/2023", 0, "repository2", "r", null);
        List<FileInformationNode> listFiles5 = new ArrayList<>();
        listFiles5.add(file9);
        listFiles5.add(file10);
        Mockito.when(folderInformation4.getName()).thenReturn("repository2");
        Mockito.when(folderInformation4.browseFolder(new File("repository2"))).thenReturn(listFiles5);

        FileInformation file11 = new FileInformation("data", "20/10/2023", 0, "repository3", "data", null);
        List<FileInformationNode> listFiles6 = new ArrayList<>();
        listFiles6.add(file11);
        Mockito.when(folderInformation5.getName()).thenReturn("repository3");
        Mockito.when(folderInformation5.browseFolder(new File("repository3"))).thenReturn(listFiles6);
        

        Scan myScan = new Scan("myRepository", 10, 2);
        Scan myScanSpy = Mockito.spy(myScan);
        Mockito.doReturn(listFiles1).when(myScanSpy).browseFolder(any(FolderInformation.class));

        Scan scan = new Scan("myRepository", 10, 2);
        scan.scanner();

        //assertFalse(scan.getFiles().isEmpty());
        //assertEquals(10, scan5.getFiles().size());
    }
}
