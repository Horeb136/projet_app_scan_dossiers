package fr.ensai.scan.model.filesInformations;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import fr.ensai.scan.model.Scan;


@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class FolderInformationTest {

        
    private FolderInformation folder;
    private FileInformation file1;
    private FileInformation file2;
    private ArrayList<FileInformation> expected = new ArrayList<>();

    File mockFileSystem = Mockito.mock(File.class);
    File mockFileSystem1 = Mockito.mock(File.class);
    File mockFileSystem2 = Mockito.mock(File.class);
    File[] files = {mockFileSystem1, mockFileSystem2};

    @BeforeAll
    private void setUp() {
        // Given
        folder = new FolderInformation("testFolder", "2024-02-07", "testRepository");
        file1 = new FileInformation("example1.txt", "2024-02-07", 100, "testRepository", "txt", null);
        file2 = new FileInformation("example2.txt", "2024-02-07", 200, "testRepository", "txt", null);
        expected.add(file1);
        Scan scan = new Scan(0, null, 0, 0, null, null);
        file1.setScan(scan);
        file2.setScan(scan);
        folder.addFile(file1);
    }

    @Test
    public void testFolderInformationConstructor() {
        // Given
        String folderName = "testFolder";
        String folderDateModif = "2024-02-07";
        String folderRepo = "testRepository";
        // When
        FolderInformation folder = new FolderInformation(folderName, folderDateModif, folderRepo);
        // Then
        assertEquals(folderName, folder.getName());
        assertEquals(folderDateModif, folder.getDateModification());
        assertEquals(folderRepo, folder.getRepository());

        // When
        folder = new FolderInformation();
        // Then
        assertNull(folder.getName());
        assertNull(folder.getDateModification());
        assertNull(folder.getRepository());
    }

    @Test
    public void testAddFileAndGetFileInformationNodes() {
        // When
        ArrayList<FileInformationNode> files = folder.getFileInformationNodes();
        // Then
        assertEquals(expected, files); // Vérifie si la taille du dossier est correctement calculée
    }

    @Test
    public void testGetSize() {
        // When
        String size = folder.getSize();
        // Then
        assertEquals("100 octets", size); // Vérifie si la taille du dossier est correctement calculée
    }
    
    @Test
    public void testBrowseFolder() {
        
        MockitoAnnotations.openMocks(this);
        
        Mockito.when(mockFileSystem.isDirectory()).thenReturn(true);
        Mockito.when(mockFileSystem.getName()).thenReturn("testFolder");
        Mockito.when(mockFileSystem.listFiles()).thenReturn(files);

        Mockito.when(mockFileSystem1.isDirectory()).thenReturn(true);
        Mockito.when(mockFileSystem1.getName()).thenReturn("subRepository");

        Mockito.when(mockFileSystem2.isDirectory()).thenReturn(false);
        Mockito.when(mockFileSystem2.getName()).thenReturn("example2.txt");

        // When
        List<FileInformationNode> fileInfoNodes = folder.browseFolder(mockFileSystem);
        // Then
        assertFalse(fileInfoNodes.isEmpty()); // Vérifie si la liste des éléments retournée n'est pas vide
        assertEquals(fileInfoNodes.size(), 2);

        // When
        fileInfoNodes = folder.browseFolder(mockFileSystem2);
        // Then
        assertTrue(fileInfoNodes.isEmpty());
    }

    @Test
    public void testToString() {
        // When
        String res = folder.toString();
        // Then
        assertTrue(res.contains("Name : testFolder\nDate Modif : 2024-02-07"));
    }
}
