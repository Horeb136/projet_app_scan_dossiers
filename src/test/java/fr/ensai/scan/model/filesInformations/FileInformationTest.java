package fr.ensai.scan.model.filesInformations;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;

import fr.ensai.scan.model.Scan;

@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class FileInformationTest {

    private FileInformation file;
    private Scan scan;

    @BeforeAll
    public void setUp() {
        // Given
        scan = new Scan(Long.valueOf(1), "DATA", 10, 2, null, null);
        file = new FileInformation("example.txt", "2024-02-07", 100, "DATA/testRepository", "txt", 5000L);
    }

    @Test
    public void testGetName() {
        // When
        String fileName = file.getName();
        // Then
        assertEquals("example.txt", fileName);
    }

    @Test
    public void testSetName() {
        // When
        file.setName("test.txt");
        String fileName = file.getName();
        // Then
        assertEquals("test.txt", fileName);
    }

    @Test
    public void testGetDateModification() {
        // When
        String fileDateModif = file.getDateModification();
        // Then
        assertEquals("2024-02-07", fileDateModif);
    }

    @Test
    public void testGetRepository() {
        // When
        String fileRepository = file.getRepository();
        // Then
        assertEquals("DATA/testRepository", fileRepository);
    }
    
    @Test
    public void testGetFileType() {
        // When
        String fileType = file.getFileType();
        // Then
        assertEquals("txt", fileType);
    }

    @Test
    public void testGetSize() {
        // When
        String size = file.getSize();
        // Then
        assertEquals("100 octets", size);
    }

    @Test
    public void testGetScanId() {
        // When
        file.setScan(scan);
        long scanId = file.getScanId();
        // Then
        assertEquals(1, scanId);

        //When
        file.setScan(new Scan());
        //Then
        assertNull(file.getScanId());
    }

    @Test
    public void testSetAndGetScan() {
        // When
        file.setScan(scan);
        Scan retrievedScan = file.getScan();
        // Then
        assertEquals(scan, retrievedScan);
    }

    @Test
    public void testGetExecutionTime() {
        // When
        String executionTime = file.getExecutionTime();
        // Then
        assertEquals("5s000ms", executionTime); // Vérifie si le temps d'exécution est correctement converti en chaîne
    }

    @Test
    public void testToString() {
        // When
        file.setScan(scan);
        String result = file.toString();

        // Then
        String expected = "File : example.txt modified 2024-02-07 of size 100 octets in DATA/testRepository and of type of txt with scan id 1";
        assertEquals(expected, result);
    }   
}
