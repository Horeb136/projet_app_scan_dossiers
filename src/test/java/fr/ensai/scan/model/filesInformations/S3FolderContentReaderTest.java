package fr.ensai.scan.model.filesInformations;


import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import fr.ensai.scan.model.Scan;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;

@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class S3FolderContentReaderTest {

    private S3FolderContentReader s3FolderContentReader;
    private FileInformation file1;
    private FileInformation file2;
    private List<FileInformation> expected = new ArrayList<>();

    @BeforeAll
    private void setUp() {
        // Given
        s3FolderContentReader = new S3FolderContentReader("testFolder", "2024-02-07", "testRepository", "testBucket");
        file1 = new FileInformation("example1.txt", "2024-02-07", 100, "testRepository", "txt", null);
        file2 = new FileInformation("example2.txt", "2024-02-07", 200, "testRepository", "txt", null);
        expected.add(file1);
        Scan scan = new Scan(0, null, 0, 0, null, null);
        file1.setScan(scan);
        file2.setScan(scan);
        s3FolderContentReader.addFile(file1);
    }

    @Test
    public void testS3FolderContentReaderConstructor() {
        // Given
        String folderName = "testFolder";
        String folderDateModif = "2024-02-07";
        String folderRepo = "testRepository";
        String bucketName = "testBucket";
        // Then
        assertEquals(folderName, s3FolderContentReader.getName());
        assertEquals(folderDateModif, s3FolderContentReader.getDateModification());
        assertEquals(folderRepo, s3FolderContentReader.getRepository());
        assertEquals(bucketName, s3FolderContentReader.getBucketName());

        // Then
        assertNull((new S3FolderContentReader()).getName());
        assertNull((new S3FolderContentReader()).getDateModification());
        assertNull((new S3FolderContentReader()).getRepository());
        assertNull((new S3FolderContentReader()).getBucketName());
    }

    @Test
    public void testAddFileAndGetFileInformationNodes() {
        // When
        List<FileInformationNode> files = s3FolderContentReader.getFileInformationNodes();
        // Then
        assertEquals(expected, files); // Vérifie si la taille du dossier est correctement calculée
    }

    @Test
    public void testGetSize() {
        // When
        String size = s3FolderContentReader.getSize();
        // Then
        assertEquals("100 octets", size); // Vérifie si la taille du dossier est correctement calculée
    }
    
    @Test
    public void testBrowseFolder() {
        
        MockitoAnnotations.openMocks(this);

        S3Client mockS3Client = Mockito.mock(S3Client.class);
        ListObjectsV2Request mockListObjectsV2Request1 = Mockito.mock(ListObjectsV2Request.class);
        ListObjectsV2Response mockListObjectsV2Response1 = Mockito.mock(ListObjectsV2Response.class);
        ListObjectsV2Request mockListObjectsV2Request2 = Mockito.mock(ListObjectsV2Request.class);
        ListObjectsV2Response mockListObjectsV2Response2 = Mockito.mock(ListObjectsV2Response.class);
        S3Object s3Object1 = Mockito.mock(S3Object.class);
        S3Object s3Object2 = Mockito.mock(S3Object.class);
        S3Object s3Object3 = Mockito.mock(S3Object.class);
        S3Object s3Object4 = Mockito.mock(S3Object.class);
        
        List<S3Object> listS3Object = new ArrayList<>();
        listS3Object.add(s3Object1);
        listS3Object.add(s3Object2);
        listS3Object.add(s3Object3);
        listS3Object.add(s3Object4);

        Mockito.when(mockS3Client.listObjectsV2(mockListObjectsV2Request1)).thenReturn(mockListObjectsV2Response1);
        Mockito.when(mockS3Client.listObjectsV2(mockListObjectsV2Request2)).thenReturn(mockListObjectsV2Response2);
        Mockito.when(mockListObjectsV2Response1.contents()).thenReturn(listS3Object);
        Mockito.when(s3Object1.key()).thenReturn("docs/ppdp/");
        Mockito.when(s3Object2.key()).thenReturn("docs/exemple.txt");
        Mockito.when(s3Object3.key()).thenReturn("docs/");
        Mockito.when(s3Object4.key()).thenReturn("docs/");
        Mockito.when(s3Object1.lastModified()).thenReturn(Instant.now());
        Mockito.when(s3Object2.lastModified()).thenReturn(Instant.now());
        Mockito.when(s3Object3.lastModified()).thenReturn(Instant.now());
        Mockito.when(s3Object4.lastModified()).thenReturn(Instant.now());
        
        // When
        List<FileInformationNode> fileInfoNodes = s3FolderContentReader.browseFolder(mockS3Client, mockListObjectsV2Request1);
        // Then
        assertFalse(fileInfoNodes.isEmpty()); // Vérifie si la liste des éléments retournée n'est pas vide
        assertEquals(4, fileInfoNodes.size());

        // When
        fileInfoNodes = s3FolderContentReader.browseFolder(mockS3Client, mockListObjectsV2Request2);
        // Then
        assertTrue(fileInfoNodes.isEmpty());
    }
}
