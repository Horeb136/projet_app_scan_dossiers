package fr.ensai.scan.model.filesInformations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class FileInformationKeyTest {
    
    private FileInformationKey key1;
    private FileInformationKey key2;
    private FileInformationKey key3;

    @BeforeAll
    public void setUp() {
        // Given
        key1 = new FileInformationKey("example.txt", 1L);
        key2 = new FileInformationKey("example.txt", 1L);
        key3 = new FileInformationKey("example.txt", 2L);
    }
    
    @Test
    public void testFileInformationKeyConstructor() {
        // Given
        String fileName = "example.txt";
        Long scanId = 1L;
        // When
        FileInformationKey key = new FileInformationKey(fileName, scanId);
        // Then
        assertEquals(fileName, key.getFileName());
        assertEquals(scanId, key.getScanId());

        // When
        key = new FileInformationKey();
        // Then
        assertNull(key.getFileName());
        assertNull(key.getScanId());
    }

    @Test
    public void testGetFileName() {
        // Given
        String fileName = "example.txt";
        // When
        String retrievedFileName = key1.getFileName();
        // Then
        assertEquals(fileName, retrievedFileName);
    }

    @Test
    public void testGetScanId() {
        // Given
        Long scanId = 1L;
        // When
        Long retrievedScanId = key1.getScanId();
        // Then
        assertEquals(scanId, retrievedScanId);
    }

    @Test
    public void testEquals() {
        // Then
        assertTrue(key1.equals(key1));
        assertTrue(key1.equals(key2));
        assertFalse(key1.equals(key3));
        assertFalse(key1.equals(new Object()));
    }

    @Test
    public void testHashCode() {
        // Then
        assertEquals(key1.hashCode(), key2.hashCode());
        assertNotEquals(key1.hashCode(), key3.hashCode());
    }

    @Test
    public void testToString() {
        // Given
        String res = "Name : example.txt, Id : 1";
        // Then
        assertEquals(res, key1.toString());

        // Given
        res = "Name : null, Id : null";
        // Then
        assertEquals(res, (new FileInformationKey()).toString());
    }
}
