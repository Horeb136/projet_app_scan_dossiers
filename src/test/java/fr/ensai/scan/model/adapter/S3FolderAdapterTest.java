package fr.ensai.scan.model.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import fr.ensai.scan.model.filesInformations.S3FolderContentReader;

@RunWith(SpringRunner.class)
@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class S3FolderAdapterTest {

    private S3FolderAdapter s3FolderAdapter;
    private S3FolderContentReader s3FolderContentReader;

    @BeforeAll
    private void setUp() {
        // Given
        s3FolderAdapter = new S3FolderAdapter(s3FolderContentReader);
    }
    
    @Test
    public void testS3FolderContentReaderConstructor() {
        // Then
        assertEquals(s3FolderContentReader, s3FolderAdapter.getS3FolderContentReader());
    }
}
