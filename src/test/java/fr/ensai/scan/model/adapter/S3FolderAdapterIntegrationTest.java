package fr.ensai.scan.model.adapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformationNode;
import fr.ensai.scan.model.filesInformations.S3FolderContentReader;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;


@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class S3FolderAdapterIntegrationTest {
    
    @Test
    public void testBrowsFolder() {
        
        S3Client mockS3Client = Mockito.mock(S3Client.class);

        ListObjectsV2Request mockListObjectsV2Request1 = Mockito.mock(ListObjectsV2Request.class);
        ListObjectsV2Response mockListObjectsV2Response1 = Mockito.mock(ListObjectsV2Response.class);

        S3Object s3Object1 = Mockito.mock(S3Object.class);
        S3Object s3Object2 = Mockito.mock(S3Object.class);
        S3Object s3Object3 = Mockito.mock(S3Object.class);
        
        List<S3Object> listS3Object = new ArrayList<>();
        listS3Object.add(s3Object1);
        listS3Object.add(s3Object2);
        listS3Object.add(s3Object3);

        Mockito.when(mockS3Client.listObjectsV2(mockListObjectsV2Request1)).thenReturn(mockListObjectsV2Response1);
        Mockito.when(mockListObjectsV2Response1.contents()).thenReturn(listS3Object);
        
        Mockito.when(s3Object1.key()).thenReturn("docs/ppdp/");
        Mockito.when(s3Object2.key()).thenReturn("docs/example.txt");
        Mockito.when(s3Object3.key()).thenReturn("docs/data");
        
        Mockito.when(s3Object1.lastModified()).thenReturn(Instant.now());
        Mockito.when(s3Object2.lastModified()).thenReturn(Instant.now());
        Mockito.when(s3Object3.lastModified()).thenReturn(Instant.now());;
        
        // Given
        S3FolderContentReader s3FolderContentReader = new S3FolderContentReader("testFolder", "2024-02-07", "testRepository", "testBucket");
        Scan scan1 = new Scan("docs", 1, 2);
        S3FolderAdapter s3FolderAdapter = new S3FolderAdapter(s3FolderContentReader);
        // When
        List<FileInformationNode> fileInfoNodes = s3FolderAdapter.browseFolder(scan1, mockS3Client, mockListObjectsV2Request1);
        // Then
        assertFalse(scan1.getFiles().isEmpty());
        assertFalse(fileInfoNodes.isEmpty());
        assertEquals(1, fileInfoNodes.size());


        // Given
        Scan scan2 = new Scan("docs", 10, 2, "example", "txt");
        // When
        fileInfoNodes = s3FolderAdapter.browseFolder(scan2, mockS3Client, mockListObjectsV2Request1);
        // Then
        assertFalse(scan2.getFiles().isEmpty());
        assertFalse(fileInfoNodes.isEmpty());
        assertEquals(1, fileInfoNodes.size());

        // Given
        Scan scan3 = new Scan("docs", 10, 2, null, "txt");
        // When
        fileInfoNodes = s3FolderAdapter.browseFolder(scan3, mockS3Client, mockListObjectsV2Request1);
        // Then
        assertFalse(scan3.getFiles().isEmpty());
        assertFalse(fileInfoNodes.isEmpty());
        assertEquals(1, fileInfoNodes.size());

        // Given
        Scan scan4 = new Scan("docs", 10, 2, "example", null);
        // When
        fileInfoNodes = s3FolderAdapter.browseFolder(scan4, mockS3Client, mockListObjectsV2Request1);
        // Then
        //assertFalse(mockScan1.getFiles().isEmpty());
        assertFalse(fileInfoNodes.isEmpty());
        assertEquals(1, fileInfoNodes.size());
    }
}
