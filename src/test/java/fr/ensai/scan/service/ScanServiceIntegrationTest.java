package fr.ensai.scan.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.repository.FileInformationRepository;
import fr.ensai.scan.repository.ScanRepository;
import fr.ensai.scan.SpringBootDemoApplication;
import fr.ensai.scan.dto.SearchInDto;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = SpringBootDemoApplication.class)
@AutoConfigureMockMvc

class ScanServiceIntegrationTest {
    Logger LOG  = LoggerFactory.getLogger(ScanServiceIntegrationTest.class);

    @Autowired
    ScanService scanService;

    @MockBean
    private ScanRepository scanRepository;

    @MockBean
    private FileInformationRepository fileInformationRepository;

    private static Optional<Scan> optionalScan1;
    private static Optional<Scan> optionalScan2;
    private static List<Scan> scansList = new ArrayList<>();

    private static Scan scan4;

    private static Iterable<FileInformation> fileInformation;
    private static FileInformation fileInformation1;


    @BeforeAll
    private static void setUp() {
        fileInformation1 = new FileInformation("titanic.csv", "20/12/2023",  145L, "DATA/EXO/", "csv", 2L);
        Scan scan1 = new Scan(Long.valueOf(1), "DATA/EXO/", 10, 2, null, null);
        fileInformation1.setScan(scan1);
        scan1.addFile(fileInformation1);

        Scan scan2 = new Scan(Long.valueOf(2), "DATA/EXO/", 5, 2, null, null);
        Scan scan3 = new Scan("DATA/EXO/", 5, 2, null, null);
        scan4 = new Scan("DATA/EXO/", 10, 2, null, null);
        
        optionalScan1 = Optional.of(scan1);
        optionalScan2 = Optional.of(scan2);

        scansList.add(scan1);
        scansList.add(scan2);
        scansList.add(scan3);
        scansList.add(scan4);

        FileInformation[] fiche = {fileInformation1};
        fileInformation = Arrays.asList(fiche);
    }

    @BeforeEach
    public void createScanSetUp() {

        MockitoAnnotations.openMocks(this);
        
        Mockito.when(scanRepository.findById(Long.valueOf(1))).thenReturn(optionalScan1); //
        Mockito.when(scanRepository.findById(Long.valueOf(2))).thenReturn(optionalScan2);
        Mockito.when(scanRepository.findById(Long.valueOf(3))).thenReturn(Optional.empty());
        Mockito.when(scanRepository.existsById(Long.valueOf(1))).thenReturn(false);
        Mockito.when(scanRepository.existsById(Long.valueOf(2))).thenReturn(true);
        Mockito.when(scanRepository.save(scansList.get(0))).thenReturn(scansList.get(0));
        Mockito.when(scanRepository.save(scansList.get(1))).thenReturn(scansList.get(1));
        Mockito.when(scanRepository.save(scansList.get(2))).thenReturn(null);
        Mockito.when(scanRepository.save(scan4)).thenReturn(scan4);

        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(1))).thenReturn(fileInformation);
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(3))).thenReturn(null);
        Mockito.when(fileInformationRepository.save(fileInformation1)).thenReturn(null);
        
        
    }

    @Test
    public void testDeleteScanById() {
        boolean deleted = scanService.deleteScanById(Long.valueOf(1));
        assertTrue(deleted);
        deleted = scanService.deleteScanById(Long.valueOf(3));
        assertFalse(deleted);
    }

    @Test
    public void testCreateScan() {
        boolean result = scanService.createScan(scansList.get(1));
        assertTrue(result);
        result = scanService.createScan(scansList.get(2));
        assertFalse(result);
        result = scanService.createScan(scansList.get(0));
        assertTrue(result);
    }

    @Test
    public void testReplayScan() {

        scan4.addFile(fileInformation1);
        Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(scan4);

        boolean result = scanService.replayScan(Long.valueOf(1));
        assertTrue(result);

        // Scan n'existant pas
        result = scanService.replayScan(Long.valueOf(4));
        assertFalse(result);
    }

    @Test
    public void testDuplicateScan() {

        SearchInDto searchInDto = new SearchInDto(10, 2, null, null);

        Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(scan4);
        boolean result = scanService.duplicateScan(Long.valueOf(2), searchInDto);
        assertTrue(result);

        // Scan n'existant pas
        System.out.println("Debut");
        result = scanService.duplicateScan(Long.valueOf(4), searchInDto);
        assertFalse(result);
    }

}