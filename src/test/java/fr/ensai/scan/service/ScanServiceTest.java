package fr.ensai.scan.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.repository.ScanRepository;

@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

class ScanServiceTest {
    Logger LOG  = LoggerFactory.getLogger(ScanServiceTest.class);

    @Autowired
    ScanService scanService;

    @MockBean
    private ScanRepository scanRepository;

    private Optional<Scan> optionalScan1;
    private Optional<Scan> optionalScan2;
    private List<Scan> scansList = new ArrayList<>();


    @BeforeAll
    private void setUp() {
        FileInformation fileInformation1 = new FileInformation("titanic.csv", "20/12/2023",  145L, "DATA", "csv", 2L);
        Scan scan1 = new Scan(Long.valueOf(1), "DATA", 10, 2, null, null);
        fileInformation1.setScan(scan1);
        scan1.addFile(fileInformation1);

        FileInformation fileInformation2 = new FileInformation("titanic.csv", "31/12/2023",  15L, "DATA/EXO/", "csv", 2L);
        Scan scan2 = new Scan(Long.valueOf(2), "DATA/EXO/", 5, 2, null, null);
        fileInformation2.setScan(scan2);
        scan2.addFile(fileInformation2);

        Scan scan3 = new Scan("DATA", 10, 2, null, null);

        optionalScan1 = Optional.of(scan1);
        optionalScan2 = Optional.of(scan2);

        scansList.add(scan1);
        scansList.add(scan2);
        scansList.add(scan3);
    }

    @SuppressWarnings("null")
    @BeforeEach
    public void createScanSetUp() {

        MockitoAnnotations.openMocks(this);
        
        Mockito.when(scanRepository.findById(Long.valueOf(1))).thenReturn(optionalScan1);
        Mockito.when(scanRepository.findById(Long.valueOf(2))).thenReturn(optionalScan2);
        Mockito.when(scanService.getAllScan()).thenReturn(scansList);
        Mockito.when(scanService.countScans()).thenReturn(Long.valueOf(scansList.size()));
        Mockito.when(scanRepository.save(scansList.get(2))).thenReturn(scansList.get(2));
    }


    @Test
    public void testGetScanById() {
        Optional<Scan> result = scanService.getScanById(Long.valueOf(1));
        assertEquals(optionalScan1, result);
    }

    @Test
    public void testGetAllScan() {
        Iterable<Scan> result = scanService.getAllScan();
        assertEquals(scansList, result);
    }

    @Test
    public void testCountScans() {
        assertEquals(scanService.countScans(), Long.valueOf(3));
    }

    @Test
    public void testCompareScans() {
        boolean result = scanService.compareScans(Long.valueOf(1), Long.valueOf(2));
        assertFalse(result);
        result = scanService.compareScans(Long.valueOf(1), Long.valueOf(1));
        assertTrue(result);
        result = scanService.compareScans(Long.valueOf(1), Long.valueOf(3));
        assertFalse(result);
    }

    @Test
    public void testGetScansStatistics() {
        Iterable<String> result = scanService.getScansStatistics(Long.valueOf(1));
        List<String> strings = new ArrayList<>(Arrays.asList("Mean folder execution time : null", "Mean file execution time : 0ms"));
        Iterable<String> expected = strings;
        assertEquals(result, expected);

        result = scanService.getScansStatistics(Long.valueOf(3));
        assertNull(result);
    }

}