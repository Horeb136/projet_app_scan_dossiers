package fr.ensai.scan.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.model.filesInformations.FileInformationKey;
import fr.ensai.scan.repository.FileInformationRepository;

@SpringBootTest
    @TestInstance(Lifecycle.PER_CLASS)

public class FileInformationServiceTest {

    Logger LOG  = LoggerFactory.getLogger(ScanServiceTest.class);

    @Autowired
    FileInformationService fileInformationService;

    @MockBean
    private FileInformationRepository fileInformationRepository;

    private Iterable<FileInformation> fileInformation;
    private List<FileInformation> files = new ArrayList<>();
    private List<FileInformation> fileList = new ArrayList<>();


    @BeforeAll
    private void setUp() {
        FileInformation fileInformation1 = new FileInformation("titanic.csv", "20/12/2023",  145L, "DATA", "csv", 2L);
        Scan scan1 = new Scan(Long.valueOf(1), "DATA", 10, 2, null, null);
        fileInformation1.setScan(scan1);
        scan1.addFile(fileInformation1);

        FileInformation fileInformation2 = new FileInformation("iris.csv", "31/12/2023",  15L, "DATA/EXO/", "csv", 2L);
        Scan scan2 = new Scan(Long.valueOf(2), "DATA/EXO/", 5, 2, null, null);
        fileInformation2.setScan(scan2);
        scan2.addFile(fileInformation2);

        files.add(fileInformation1);

        FileInformation[] fiche = {fileInformation1};
        fileInformation = Arrays.asList(fiche);

        fileList.add(fileInformation1);
        fileList.add(fileInformation2);
    }

    @BeforeEach
    public void createFileInformations() {

        MockitoAnnotations.openMocks(this);
        
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(1))).thenReturn(fileInformation);
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(3))).thenReturn(null);
        Mockito.when(fileInformationRepository.findAll()).thenReturn(fileList);
        Mockito.when(fileInformationRepository.count()).thenReturn(Long.valueOf(fileList.size()));
        Mockito.when(fileInformationRepository.findByName("titanic.csv")).thenReturn(files);
        Mockito.when(fileInformationRepository.findByFileType("csv")).thenReturn(fileList);
        Mockito.when(fileInformationRepository.save(files.get(0))).thenReturn(files.get(0));
        Mockito.when(fileInformationRepository.save(new FileInformation())).thenReturn(null);
        Mockito.when(fileInformationRepository.saveAll(files)).thenReturn(files);
        Mockito.when(fileInformationRepository.saveAll(new ArrayList<>())).thenReturn(null);
        Mockito.when(fileInformationRepository.findAllById(null)).thenReturn(null);
    }


    @Test
    public void testGetFileInformationByScanId() {
        Iterable<FileInformation> result = fileInformationService.getFileInformationByScanId(Long.valueOf(1));
        assertEquals(files, result);
    }

    @Test
    public void testGetAllFileInformation() {
        Iterable<FileInformation> result = fileInformationService.getAllFileInformation();
        assertEquals(fileList, result);
    }

    @Test
    public void testGetFileInformationByName() {
        Iterable<FileInformation> result = fileInformationService.getFileInformationByName("titanic.csv");
        assertEquals(files, result);
    }

    @Test
    public void testGetFileInformationByType() {
        Iterable<FileInformation> result = fileInformationService.getFileInformationByType("csv");
        assertEquals(fileList, result);
    }

    @Test
    public void testDeleteFileInformationByScanId() {

        assertTrue(fileInformationService.deleteFileInformationByScanId(Long.valueOf(1))); // Fichiers supprimés
        assertFalse(fileInformationService.deleteFileInformationByScanId(Long.valueOf(3))); // Scan n'existant pas

        // Scan existant mais ses fichiers n'ont pas été supprimés
        Scan scan4 = new Scan(Long.valueOf(4), "test", 10, 2, null, null);
        FileInformation fileInfo1 = new FileInformation("test.csv", "20/12/2023",  145L, "DATA", "csv", 2L);
        Iterable<FileInformation> myFiles;
        FileInformation[] fiche = {fileInfo1};
        myFiles = Arrays.asList(fiche);
        fileInfo1.setScan(scan4);
        scan4.addFile(fileInfo1);

        Iterable<FileInformationKey> keysList;
        FileInformationKey[] keys = {(new FileInformationKey("test.csv", Long.valueOf(4)))};
        keysList = Arrays.asList(keys);
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(4))).thenReturn(myFiles);
        Mockito.when(fileInformationRepository.findAllById(keysList)).thenReturn(myFiles);
        assertFalse(fileInformationService.deleteFileInformationByScanId(Long.valueOf(4)));

        // Scan existant mais sans fichiers
        myFiles = Collections.emptyList();
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(4))).thenReturn(myFiles);
        assertTrue(fileInformationService.deleteFileInformationByScanId(Long.valueOf(4)));
    }

    @Test
    public void testCountFileInformation() {
        assertEquals(fileInformationService.countFileInformation(), Long.valueOf(fileList.size()));
    }

    @Test
    public void testCreateFileInformation() {
        boolean result = fileInformationService.createFileInformation(files.get(0));
        assertTrue(result);
        result = fileInformationService.createFileInformation(new FileInformation());
        assertFalse(result);
    }

    @Test
    public void testCreateFileInformations() {
        boolean result = fileInformationService.createFileInformations(files);
        assertTrue(result);
        result = fileInformationService.createFileInformations(new ArrayList<>());
        assertFalse(result);
    }

    @Test
    public void testMeanFileExecutionTime() {
        // Scan qui n'existe pas
        assertEquals(0.0, fileInformationService.meanFileExecutionTime(Long.valueOf(5)));

        // Given
        Scan scan4 = new Scan(Long.valueOf(4), "test", 10, 2, null, null);
        FileInformation fileInfo1 = new FileInformation("test.csv", "20/12/2023",  145L, "DATA", "csv", 2L);
        FileInformation fileInfo2 = new FileInformation("test.java", "20/12/2023",  145L, "DATA", "java", 4L);
        Iterable<FileInformation> myFiles;
        FileInformation[] fiche = {fileInfo1, fileInfo2};
        myFiles = Arrays.asList(fiche);
        fileInfo1.setScan(scan4);
        scan4.addFile(fileInfo1);
        fileInfo2.setScan(scan4);
        scan4.addFile(fileInfo2);
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(4))).thenReturn(myFiles);
        // When
        double meanExecTime = fileInformationService.meanFileExecutionTime(Long.valueOf(4));
        //Then
        assertEquals(3.0, meanExecTime);
    }
    
}
