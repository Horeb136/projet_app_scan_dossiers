package fr.ensai.scan.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)

public class DataFormatterTest {

    @Test
    public void testConvertSize() {
        assertEquals("0 octet", DataFormatter.convertSize(null));
        assertEquals("1.1 Go", DataFormatter.convertSize(Long.valueOf(1_187_149_471)));
        assertEquals("35.0 Mo", DataFormatter.convertSize(Long.valueOf(36_700_160)));
        assertEquals("15.4 Ko", DataFormatter.convertSize(Long.valueOf(15_800)));
        assertEquals("450 octets", DataFormatter.convertSize(Long.valueOf(450)));
    }

    @Test
    public void testDeConvertSize() {
        assertEquals(Long.valueOf(1_181_116_006), DataFormatter.deConvertSize("1.1 Go"));
        assertEquals(Long.valueOf(20_971_520), DataFormatter.deConvertSize("20 Mo"));
        assertEquals(Long.valueOf(77_107), DataFormatter.deConvertSize("75.3 Ko"));
        assertEquals(Long.valueOf(56), DataFormatter.deConvertSize("56 octets"));
    }

    @Test
    public void testConvertExecTime() {
        assertNull(DataFormatter.convertExecTime(null));
        assertEquals("1h15m35s200ms", DataFormatter.convertExecTime(Long.valueOf(4_535_200)));
        assertEquals("36m00s015ms", DataFormatter.convertExecTime(Long.valueOf(2_160_015)));
        assertEquals("28s000ms", DataFormatter.convertExecTime(Long.valueOf(28_000)));
        assertEquals("145ms", DataFormatter.convertExecTime(Long.valueOf(145)));
    }

    @Test
    public void testConvertStringToDuration() {
        assertNull(DataFormatter.convertStringToDuration(null));
        assertEquals(Long.valueOf(4_535_200), DataFormatter.convertStringToDuration("1h15m35s200ms"));
        assertEquals(Long.valueOf(2_160_015), DataFormatter.convertStringToDuration("36m00s015ms"));
        assertEquals(Long.valueOf(28_000), DataFormatter.convertStringToDuration("28s000ms"));
        assertEquals(Long.valueOf(145), DataFormatter.convertStringToDuration("145ms"));
        assertThrows(IllegalArgumentException.class, () -> DataFormatter.convertStringToDuration(""));
        assertThrows(IllegalArgumentException.class, () -> DataFormatter.convertStringToDuration("36m0s015ms"));
    }

    @Test
    public void testConvertDurationToString() {
        assertNull(DataFormatter.convertDurationToString(null));
        assertEquals("1h15m35s200ms", DataFormatter.convertDurationToString(4_535_200.0));
        assertEquals("36m00s015ms", DataFormatter.convertDurationToString(2_160_015.0));
        assertEquals("28s000ms", DataFormatter.convertDurationToString(28_000.0));
        assertEquals("145ms", DataFormatter.convertDurationToString(Double.valueOf(145.0)));
    }
}
