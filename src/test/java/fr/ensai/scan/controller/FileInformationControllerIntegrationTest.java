package fr.ensai.scan.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import fr.ensai.scan.SpringBootDemoApplication;
import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.repository.FileInformationRepository;

@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.MOCK,
  classes = SpringBootDemoApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
  locations = "classpath:application-integrationtest.properties")

public class FileInformationControllerIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private FileInformationRepository fileInformationRepository;

    private static List<FileInformation> files = new ArrayList<>();
    private static List<FileInformation> fileList = new ArrayList<>();


    @BeforeAll
    private static void setUp() {
        FileInformation fileInformation1 = new FileInformation("titanic.csv", "20/12/2023",  145L, "DATA", "csv", 2L);
        Scan scan1 = new Scan(Long.valueOf(1), "DATA", 10, 2, null, null);
        fileInformation1.setScan(scan1);
        scan1.addFile(fileInformation1);

        FileInformation fileInformation2 = new FileInformation("iris.csv", "31/12/2023",  15L, "DATA/EXO/", "csv", 2L);
        Scan scan2 = new Scan(Long.valueOf(2), "DATA/EXO/", 5, 2, null, null);
        fileInformation2.setScan(scan2);
        scan2.addFile(fileInformation2);

        files.add(fileInformation1);

        fileList.add(fileInformation1);
        fileList.add(fileInformation2);
    }

    @BeforeEach
    public void createFileInformations() {

        MockitoAnnotations.openMocks(this);
        
        Mockito.when(fileInformationRepository.findByScan_id(Long.valueOf(1))).thenReturn(files);
        Mockito.when(fileInformationRepository.findAll()).thenReturn(fileList);
        Mockito.when(fileInformationRepository.count()).thenReturn(Long.valueOf(fileList.size()));
        Mockito.when(fileInformationRepository.findByName("titanic.csv")).thenReturn(files);
        Mockito.when(fileInformationRepository.findByFileType("csv")).thenReturn(fileList);
    }

    @SuppressWarnings("null")
    @Test
    public void testGetAllFileInformation() throws Exception {
      mvc.perform(get("/files/"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[1].name", is("iris.csv")))
        .andExpect(jsonPath("$[1].dateModification", is("31/12/2023")))
        .andExpect(jsonPath("$[1].size", is("15 octets")))
        .andExpect(jsonPath("$[1].repository", is("DATA/EXO/")))
        .andExpect(jsonPath("$[1].fileType", is("csv")))
        .andExpect(jsonPath("$[1].scanId", is(2)));
    }

    @SuppressWarnings("null")
    @Test
    public void testGetFileInformationByScanId() throws Exception {
      mvc.perform(get("/files/scan/{id}", Long.valueOf(1)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].name", is("titanic.csv")))
        .andExpect(jsonPath("$[0].dateModification", is("20/12/2023")))
        .andExpect(jsonPath("$[0].size", is("145 octets")))
        .andExpect(jsonPath("$[0].repository", is("DATA")))
        .andExpect(jsonPath("$[0].fileType", is("csv")))
        .andExpect(jsonPath("$[0].scanId", is(1)));
    }

    @Test
    public void testCountFileInformation() throws Exception {
      mvc.perform(get("/files/count"))
        .andExpect(status().isOk())
        .andExpect(content().string("2"));
    }

    @SuppressWarnings("null")
    @Test
    public void testGetFileInformationByName() throws Exception {
      mvc.perform(get("/files/name/{name}", "titanic.csv"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].name", is("titanic.csv")))
        .andExpect(jsonPath("$[0].dateModification", is("20/12/2023")))
        .andExpect(jsonPath("$[0].size", is("145 octets")))
        .andExpect(jsonPath("$[0].repository", is("DATA")))
        .andExpect(jsonPath("$[0].fileType", is("csv")))
        .andExpect(jsonPath("$[0].scanId", is(1)));
    }

    @SuppressWarnings("null")
    @Test
    public void testGetFileInformationByType() throws Exception {
      mvc.perform(get("/files/type/{fileType}", "csv"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].name", is("titanic.csv")))
        .andExpect(jsonPath("$[0].dateModification", is("20/12/2023")))
        .andExpect(jsonPath("$[0].size", is("145 octets")))
        .andExpect(jsonPath("$[0].repository", is("DATA")))
        .andExpect(jsonPath("$[0].fileType", is("csv")))
        .andExpect(jsonPath("$[0].scanId", is(1)));
    }
}
