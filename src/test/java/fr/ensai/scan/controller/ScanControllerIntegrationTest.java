package fr.ensai.scan.controller;


import static org.mockito.ArgumentMatchers.any;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import fr.ensai.scan.SpringBootDemoApplication;
import fr.ensai.scan.model.Scan;
import fr.ensai.scan.model.filesInformations.FileInformation;
import fr.ensai.scan.repository.ScanRepository;
import fr.ensai.scan.service.ScanService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;


@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.MOCK,
  classes = SpringBootDemoApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
  locations = "classpath:application-integrationtest.properties")

public class ScanControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ScanService scanService;

    @MockBean
    private ScanRepository scanRepository;

    private static Optional<Scan> optionalScan1;
    private static Optional<Scan> optionalScan2;
    private static List<Scan> scansList = new ArrayList<>();


    @BeforeAll
    private static void setUp() {
        FileInformation fileInformation1 = new FileInformation("titanic.csv", "20/12/2023",  145L, "DATA", "csv", 2L);
        Scan scan1 = new Scan(Long.valueOf(1), "DATA", 10, 2, null, null);
        fileInformation1.setScan(scan1);
        scan1.addFile(fileInformation1);

        FileInformation fileInformation2 = new FileInformation("titanic.csv", "31/12/2023",  15L, "DATA", "csv", 2L);
        Scan scan2 = new Scan(Long.valueOf(2), "DATA", 5, 2, null, null);
        fileInformation2.setScan(scan2);
        scan2.addFile(fileInformation2);

        Scan scan3 = new Scan("DATA", 10, 2, null, null);

        optionalScan1 = Optional.of(scan1);
        optionalScan2 = Optional.of(scan2);

        scansList.add(scan1);
        scansList.add(scan2);
        scansList.add(scan3);
    }

    @SuppressWarnings("null")
    @BeforeEach
    public void createScanSetUp() {

        MockitoAnnotations.openMocks(this);
        
        Mockito.when(scanRepository.findById(Long.valueOf(1))).thenReturn(optionalScan1);
        Mockito.when(scanRepository.findById(Long.valueOf(2))).thenReturn(optionalScan2);
        Mockito.when(scanRepository.findById(Long.valueOf(3))).thenReturn(Optional.empty());
        Mockito.when(scanRepository.findById(Long.valueOf(4))).thenReturn(optionalScan2);
        Mockito.when(scanRepository.existsById(Long.valueOf(4))).thenReturn(true);
        Mockito.when(scanService.getAllScan()).thenReturn(scansList);
        Mockito.when(scanService.countScans()).thenReturn(Long.valueOf(scansList.size()));
        Mockito.when(scanRepository.save(scansList.get(0))).thenReturn(scansList.get(0));
        Mockito.when(scanRepository.save(scansList.get(2))).thenReturn(scansList.get(2));
    }

    @SuppressWarnings("null")
    @Test
    public void testGetAllScans() throws Exception {
      mvc.perform(get("/scan/"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].repository", is("DATA")))
        .andExpect(jsonPath("$[0].maxFiles", is(10)))
        .andExpect(jsonPath("$[0].maxDepth", is(2)))
        .andExpect(jsonPath("$[0].id", is(1)));
    }

    @SuppressWarnings("null")
    @Test
    public void testGetScanById() throws Exception {
      mvc.perform(get("/scan/{id}", Long.valueOf(1)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.repository", is("DATA")))
        .andExpect(jsonPath("$.maxFiles", is(10)))
        .andExpect(jsonPath("$.maxDepth", is(2)))
        .andExpect(jsonPath("$.id", is(1)));
    }

    @SuppressWarnings("null")
	  @Test
    public void testCreateScan() throws Exception {
      Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(scansList.get(2));
      String requestBody = "{\"repository\": \"DATA\", \"maxFiles\": 10, \"maxDepth\": 2}";
      mvc.perform(post("/scan/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(requestBody)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string("Scan created\n"));

      Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(null);
      requestBody = "{\"repository\": \"DATA\", \"maxFiles\": 10, \"maxDepth\": 2}";
      mvc.perform(post("/scan/create")
        .contentType(MediaType.APPLICATION_JSON)
        .content(requestBody)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound())
        .andExpect(content().string(""));
    }

    @SuppressWarnings("null")
    @Test
    public void testCountScans() throws Exception {
      mvc.perform(get("/scan/count"))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("3")));
    }

    @SuppressWarnings("null")
    @Test
    public void testDeleteScanById() throws Exception {
      mvc.perform(delete("/scan/delete/{id}", Long.valueOf(1)))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("Scan 1 deleted\n")));

      mvc.perform(delete("/scan/delete/{id}", Long.valueOf(3)))
        .andExpect(status().isNotFound())
        .andExpect(content().string(""));

      mvc.perform(delete("/scan/delete/{id}", Long.valueOf(4)))
        .andExpect(status().isNotFound())
        .andExpect(content().string(""));
    }

    @SuppressWarnings("null")
	  @Test
    public void testDuplicateScan() throws Exception {
      Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(scansList.get(2));
      
      String requestBody = "{\"maxFiles\": 5, \"maxDepth\": 2, \"typeFilter\": \"txt\", \"fileFilter\": \"code\"}";
      mvc.perform(post("/scan/duplicate/{id}", Long.valueOf(1))
        .contentType(MediaType.APPLICATION_JSON)
        .content(requestBody)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("Scan duplicated\n")));
      
      requestBody = "{\"maxFiles\": 5, \"maxDepth\": 2}";
      mvc.perform(post("/scan/duplicate/{id}", Long.valueOf(3))
        .contentType(MediaType.APPLICATION_JSON)
        .content(requestBody)
        .accept(MediaType.APPLICATION_JSON))  
        .andExpect(status().isNotFound())
        .andExpect(content().string(equalTo("")));

      Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(null);
      mvc.perform(post("/scan/duplicate/{id}", Long.valueOf(1))
        .contentType(MediaType.APPLICATION_JSON)
        .content(requestBody)
        .accept(MediaType.APPLICATION_JSON)) 
        .andExpect(status().isNotFound())
        .andExpect(content().string(equalTo("")));
    }

    @SuppressWarnings("null")
    @Test
    public void testReplayScan() throws Exception {
      Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(scansList.get(2));
      
      mvc.perform(post("/scan/replay/{id}", Long.valueOf(1)))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("Scan 1 replayed\n")));

      mvc.perform(post("/scan/replay/{id}", Long.valueOf(3)))
        .andExpect(status().isNotFound())
        .andExpect(content().string(""));

      Mockito.when(scanRepository.save(any(Scan.class))).thenReturn(null);
      
      mvc.perform(post("/scan/replay/{id}", Long.valueOf(1)))
        .andExpect(status().isInternalServerError())
        .andExpect(content().string(""));
    }

    @SuppressWarnings("null")
    @Test
    public void testCompareScans() throws Exception {
      mvc.perform(get("/scan/compare/{id1}/{id2}", Long.valueOf(1), Long.valueOf(2)))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("false")));

        mvc.perform(get("/scan/compare/{id1}/{id2}", Long.valueOf(1), Long.valueOf(1)))
        .andExpect(status().isOk())
        .andExpect(content().string(equalTo("true")));
    }

    @SuppressWarnings("null")
    @Test
    public void testGetScansStatistics() throws Exception {
      mvc.perform(get("/scan/statistics/{id}", Long.valueOf(1)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0]", is("Mean folder execution time : null")))
        .andExpect(jsonPath("$[1]", is("Mean file execution time : 0ms")));
    }

}