# Application springboot de scan des répertoires en local et dans un bucket S3 de AWS

## Objectif et organisation

### Objectif

L'objectif de cette application est de scanner des systèmes de fichiers à partir d'un répertoire parent et récupère pour chaque fichier :

- nom du fichier
- date de modification
- poids
- type de fichier
- répertoire contenant

Un scan de répertoire contient les informations suivantes :
- identifiant de scan
- nombre maximum de fichiers
- profondeur maximale dans l’arborescence
- filtre sur le nom du fichier
- filtre sur le type de fichier
- date du scan
- temps d’exécution d’un scan
- informations sur chaque fichier

### Organisation du code

![Structure](img/arborescence.png)

_Figure 1 : Structure du code_

- [] controller : contient les classes de contrôleurs qui reçoivent les requêtes HTTP et fournissent les réponses appropriées. Il contient les points d'entrée de l'API et orchestrent les actions à effectuer.
- []        dto : contient les classes utilisées pour transférer des données entre les différentes couches de notre application, précisement entre la couche controller et la couche service.
- []      model : implémente les différentes classes du diagramme de classe qui représentent les entités métier de notre application.
- [] repository : contient les interfaces des repositories, qui fournissent un accès aux données persistantes.
- []    service : contient les classes de services qui encapsulent la logique métier de l'application. Les services interagissent avec les repositories pour effectuer des opérations sur les données, appliquent des règles métier et orchestrent les différentes opérations nécessaires pour répondre aux requêtes des contrôleurs ;
- []      utils : contient les classes utilitaires qui fournissent des fonctionnalités réutilisables à travers l'application. Il contient des méthodes utilitaires, des classes de manipulation de chaînes, de temps, etc.

## Démarrage rapide

### installer java 17
	 ubuntu : apt install openjdk-17-jdk openjdk-17-jre
	 mac : https://docs.oracle.com/en/java/javase/17/install/installation-jdk-macos.html#GUID-2FE451B0-9572-4E38-A1A5-568B77B146DE
### vérifier en lançant 
	java --version


### cloner le dépôt git :

```bash
git clone https://gitlab.com/Horeb136/projet_app_scan_dossiers.git
```

### Se placer dans le dossier de l'application, via la commande:

```bash
cd projet_app_scan_dossiers
```

### Lancer un conteneur postgres

* Port : 5433
* Base de données : db_springboot_exercices
* Utilisateur : db_user
* Mot de passe : db_password

```bash
docker run -p 5433:5432 --name NOM_DU_CONTAINER -e POSTGRES_PASSWORD=db_password -e POSTGRES_USER=db_user -e POSTGRES_DB=db_springboot_exercices -d postgres:latest
```

### lancer l'application

```bash
./mvnw spring-boot:run
```

### Lancer les tests

```bash
./mvnw integration-test
```

### Utilisation de l'application

L’application permet les fonctionnalités suivantes :

#### Visualisation de tous les scan

```bash
curl -X GET http://localhost:5000/scan/
```

#### Visualisation d'un scan particulier

```bash
curl -X GET http://localhost:5000/scan/{id}
```

#### Obtenir le nombre total de scans

```bash
curl -X GET http://localhost:5000/scan/count
```

#### Créer un nouveau scan

```bash
curl -X POST -H "Content-Type: application/json" -d '{
  "repository": "",
  "maxFiles": ,
  "maxDepth": ,
  "typeFilter": ,
  "fileFilter": ""
}' http://localhost:5000/scan/create
```

```bash
curl -X POST -H "Content-Type: application/json" -d '{
  "bucketName": "",
  "repository": "",
  "maxFiles": ,
  "maxDepth": ,
  "typeFilter": "",
  "fileFilter": ""
}' http://localhost:5000/scan/create
```

#### Supprimer un scan

```bash
curl -X DELETE http://localhost:5000/scan/delete/{id}
```

#### Dupliquer un scan

```bash
curl -X POST -H "Content-Type: application/json" -d '{
  "maxFiles": ,
  "maxDepth": ,
  "typeFilter": "",
  "fileFilter": ""
}' http://localhost:5000/scan/duplicate/{id}
```

#### Rejouer un scan

```bash
curl -X POST http://localhost:5000/scan/replay/{id}
```

#### Comparer deux scans

```bash
curl -X GET http://localhost:5000/scan/compare/{id1}/{id2}
```

#### Obtenir les statistiques d'un scan

```bash
curl -X GET http://localhost:5000/scan/statistics/{id}
```

#### Afficher tous les fichiers

```bash
curl -X GET http://localhost:5000/files/
```

#### Afficher les fichiers pour un scan particulier

```bash
curl -X GET http://localhost:5000/files/scan/{id}
```

#### Afficher les fichiers de nom ou de type

```bash
curl -X GET http://localhost:5000/files/name/{name}
```

```bash
curl -X GET http://localhost:5000/files/type/{type}
```

#### Obtenir le nombre total de fichiers

```bash
curl -X GET http://localhost:5000/files/count
```





## Auteurs et remerciements

Nos remerciements à M. Alban GREAU pour son aide et son soutien.
Les auteurs (élèves 3A ENSAI en DSID)

- Julien SAWADOGO
- Horeb SEIDOU


## License

Appartient aux auteurs et à l'ENSAI.
