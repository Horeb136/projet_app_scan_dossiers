FROM eclipse-temurin:17-jdk-alpine
VOLUME /tmp
RUN mkdir /app
WORKDIR /app

#COPY .m2/ .m2/
COPY .mvn/ .mvn/
COPY mvnw ./
COPY pom.xml ./

RUN ./mvnw dependency:resolve 

COPY src ./src/
#COPY src/main/resources/application-local.properties ./src/main/resources/application.properties
#  -DskipTests
RUN ./mvnw clean install

EXPOSE 5000

# ENTRYPOINT ["java","-jar","app.jar"]

ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=dev", "./target/scan-0.0.1-SNAPSHOT.jar"]